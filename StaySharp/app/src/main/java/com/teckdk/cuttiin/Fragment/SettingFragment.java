package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Main.HistoryActivity;
import com.teckdk.cuttiin.Main.LoginActivity;
import com.teckdk.cuttiin.Main.MainCustomActivity;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class SettingFragment extends BaseFragment {

    Context context;

    ImageView imvBack;
    LinearLayout lytHistory, lytChangePassword, lytLogout;

    public SettingFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_setting, container, false);

        imvBack = (ImageView)fragment.findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        lytHistory = (LinearLayout)fragment.findViewById(R.id.lytHistory);
        lytHistory.setOnClickListener(this);

        lytChangePassword = (LinearLayout)fragment.findViewById(R.id.lytChangePassword);
        lytChangePassword.setOnClickListener(this);

        lytLogout = (LinearLayout)fragment.findViewById(R.id.lytLogout);
        lytLogout.setOnClickListener(this);

        return fragment;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                ((MainCustomActivity)context).showProfileFragment();
                break;
            case R.id.lytHistory:
                context.startActivity(new Intent(context, HistoryActivity.class));
                break;
            case R.id.lytChangePassword:
                ((MainCustomActivity)context).showResetPasswordFragment();
                break;
            case R.id.lytLogout:
                Preference.getInstance().put(context, PrefConst.PREFKEY_USEREMAIL, "");
                Preference.getInstance().put(context, PrefConst.PREFKEY_USERPWD, "");
                Commons.password = "";
                context.startActivity(new Intent((Activity) context, LoginActivity.class)); ((Activity) context).finish();
                FirebaseAuth.getInstance().signOut();
                break;
        }
    }
}
