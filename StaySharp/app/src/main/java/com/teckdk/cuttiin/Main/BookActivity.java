package com.teckdk.cuttiin.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class BookActivity extends BaseActivity {

    ImageView imvBack, imvPlus, imvMinus;
    TextView txvCnt, txvNext;
    Button btnUploadPicture;

    int cnt = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        loadLayout();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        imvPlus = (ImageView)findViewById(R.id.imvPlus);
        imvPlus.setOnClickListener(this);

        imvMinus = (ImageView)findViewById(R.id.imvMinus);
        imvMinus.setOnClickListener(this);

        txvCnt = (TextView)findViewById(R.id.txvCnt);

        txvNext = (TextView)findViewById(R.id.txvNext);
        txvNext.setOnClickListener(this);

        btnUploadPicture = (Button)findViewById(R.id.btnUploadPicture);
        btnUploadPicture.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
            case R.id.imvMinus:
                if (cnt > 0){
                    cnt--;
                    txvCnt.setText(String.valueOf(cnt));
                }
                break;
            case R.id.imvPlus:
                cnt++;
                txvCnt.setText(String.valueOf(cnt));
                break;
            case R.id.txvNext:
                startActivity(new Intent(this, SelectBookDateActivity.class));
                break;
            case R.id.btnUploadPicture:
                startActivity(new Intent(this, UploadPictureActivity.class));
                break;
        }
    }
}
