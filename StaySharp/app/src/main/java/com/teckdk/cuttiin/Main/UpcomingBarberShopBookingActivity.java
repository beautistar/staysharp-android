package com.teckdk.cuttiin.Main;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.teckdk.cuttiin.Adapter.BookListAdapter;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.Bookings;
import com.teckdk.cuttiin.Models.CombineCustomerBookingModel;
import com.teckdk.cuttiin.Models.Payments;
import com.teckdk.cuttiin.Models.ServiceModel;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.R;

public class UpcomingBarberShopBookingActivity extends BaseActivity {

    BookListAdapter mUpcomingListAdapter;

    ImageView imvBack;
    ListView lstUpcomingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_barber_shop_booking);

        loadLayout();
    }

    private void loadLayout() {

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        mUpcomingListAdapter = new BookListAdapter(this);
        lstUpcomingList = (ListView)findViewById(R.id.lstUpcomingList);
        lstUpcomingList.setAdapter(mUpcomingListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mUpcomingListAdapter.clearAll();

        getUpcomingList();
    }

    private void getUpcomingList() {

        showProgress();

        Commons.BOOKING_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                            Bookings mBooking = postSnapshot.getValue(Bookings.class);
                            final String customerIDXOX = mBooking.getBookedBarberShopID(),
                                    userIDXOX = Commons.mUserModel.getUniqueID(),
                                    payedBook = mBooking.getPaymentID(),
                                    barberIDX = mBooking.getBookedBarberID(),
                                    bookStartString = mBooking.getBookingStartTime(),
                                    bookClandar = mBooking.getCalendar(),
                                    bookTizne = mBooking.getTimezone(),
                                    bookLocal = mBooking.getLocal(),
                                    bookServiceUUID = mBooking.getBookedServiceID(),
                                    bookkUniqueIDX = mBooking.getBookingID(),
                                    bookTotalTmx = getConfirmedTotalTime(postSnapshot.getValue().toString()),
                                    checkComplete = mBooking.getIsCompleted(),
                                    shopID = mBooking.getBookedBarberShopID();

                            if (customerIDXOX.equals(userIDXOX) && !payedBook.equals(Constants.NIL) && checkComplete.equals(Constants.YES)){
                                Commons.PAYMENTS_REF.child(shopID).child(payedBook)
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                Payments mPayment = dataSnapshot.getValue(Payments.class);
                                                String payConfirm = mPayment.getPayment_aq_status_msg(),
                                                        payQuickOayConfirm = mPayment.getPayment_qp_status_msg();

                                                if (payConfirm.equals(Constants.APPROVED) && payQuickOayConfirm.equals(Constants.APPROVED)){
                                                    Commons.DATABASE_USER_REF.child(barberIDX)
                                                            .addValueEventListener(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                                    UserModel mUser = dataSnapshot.getValue(UserModel.class);
                                                                    final String barberShopName = mUser.getBarberShopID(),
                                                                            barberName = mUser.getFirstName();

                                                                    Commons.SERVICE_REF.child(barberShopName).child(bookServiceUUID)
                                                                            .addValueEventListener(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                                                    ServiceModel mService = dataSnapshot.getValue(ServiceModel.class);
                                                                                    String serviceImageURL = mService.getServiceImageUrl();
                                                                                    CombineCustomerBookingModel mCombineCustomerBooking = new CombineCustomerBookingModel(
                                                                                            serviceImageURL, barberName, bookkUniqueIDX, bookTotalTmx, bookStartString,
                                                                                            bookTizne, bookClandar, bookLocal, barberShopName);

                                                                                    mUpcomingListAdapter.add(mCombineCustomerBooking);
                                                                                }

                                                                                @Override
                                                                                public void onCancelled(@NonNull DatabaseError databaseError) { }
                                                                            });
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) { }
                                                            });
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                                        });
                            }
                        }

                        closeProgress();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private String getConfirmedTotalTime(String temp){
        String[] parsedString = temp.split(",");
        String resultStr = "";

        for (String str : parsedString){
            if (str.contains("ConfirmedTotalTime")){
                resultStr = (str.split("="))[1];
            }
        }

        return resultStr;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
        }
    }
}
