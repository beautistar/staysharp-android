package com.teckdk.cuttiin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.ServiceModel;
import com.teckdk.cuttiin.R;

import java.util.ArrayList;

/**
 * Created by STS on 5/27/2018.
 */

public class ServiceAdapter extends BaseAdapter {

    Context context;

    ArrayList<ServiceModel> allData = new ArrayList<>();

    public ServiceAdapter(Context _context) {
        this.context = _context;
    }

    public void refresh (ArrayList<ServiceModel> data){
        this.allData.clear();
        this.allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public ServiceModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){
            holder = new CustomHolder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_service, parent, false);

            holder.setId(convertView);
            holder.setData(allData.get(position));

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    class  CustomHolder{

        RoundedImageView imvPhoto;
        TextView txvServiceTitle, txvDescription, txvServiceTime, txvPrice;

        private void setId(View view){
            imvPhoto = (RoundedImageView)view.findViewById(R.id.imvPhoto);
            txvServiceTitle = (TextView)view.findViewById(R.id.txvServiceTitle);
            txvDescription = (TextView)view.findViewById(R.id.txvDescription);
            txvServiceTime = (TextView)view.findViewById(R.id.txvServiceTime);
            txvPrice = (TextView)view.findViewById(R.id.txvPrice);
        }

        private void setData(ServiceModel mServiceModel){
            if (!mServiceModel.getServiceImageUrl().equals(Constants.NIL) && mServiceModel.getServiceImageUrl().length() > 0){
                Picasso.get().load(mServiceModel.getServiceImageUrl()).into(imvPhoto);
            }

            txvServiceTitle.setText(mServiceModel.getServiceTitle());
            txvDescription.setText(mServiceModel.getShortDescription());
            txvServiceTime.setText(mServiceModel.getEstimatedTime() + "min");
            txvPrice.setText(mServiceModel.getServiceCost() + "-");
        }
    }
}
