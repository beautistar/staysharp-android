package com.teckdk.cuttiin.Main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.BarberModel;
import com.teckdk.cuttiin.Models.BarberServiceModel;
import com.teckdk.cuttiin.Models.BarberShopBarbers;
import com.teckdk.cuttiin.Models.ServiceBarberModel;
import com.teckdk.cuttiin.Models.ServiceModel;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.BitmapUtils;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import android.icu.util.Calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class AddBarberActivity extends BaseActivity {

    OfferedServicesAdapter mOfferedServicesAdapter;

    EditText edtFirstName, edtLastName, edtEmail;
    Button btnDone, btnHide;
    RoundedImageView imvPhoto;
    TextView txvError;
    ListView lstOfferedServices;
    ImageView imvBack;

    FirebaseOptions options;
    FirebaseApp secondaryApp;
    FirebaseDatabase secondaryDatabase;
    FirebaseAuth secondAuth;
    FirebaseStorage storage;
    StorageReference photoRef;

    String firstName = "", lastName = "", email = "", password = "";
    private Uri imageCaptureUri;
    String photoPath = "", photoUrl = "", profileImageName = "", role = "", barberShopId ="", mobileNumber = ""
            , dateAccountCreated = "", userTimezone = "", userCalender = "", userLocal = "", rating = "", isDeleted = "";

    boolean showBackImage = false;
    ArrayList<String> selectedServiceID = new ArrayList<>();
    String newUserId = "", selectedBarberId = "";
    ArrayList<ServiceBarberModel> serviceBarberArrayForLink = new ArrayList<>();
    ArrayList<BarberServiceModel> barberServiceArrayForLink = new ArrayList<>();
    ArrayList<ServiceModel> services = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_barber);

        try{
            showBackImage = getIntent().getBooleanExtra(Constants.SHOW_BACK_IMAGE, false);
        }catch (Exception e){}

        try {
            selectedBarberId = getIntent().getStringExtra(Constants.BARBER_ID);
        }catch (Exception e){

        }
        loadLayout();
        init();
    }

    @SuppressLint("NewApi")
    private void init() {
        password = Constants.PASSWORD;
        storage = FirebaseStorage.getInstance();
        role = Constants.BARBER_STAFF;
        barberShopId = FirebaseAuth.getInstance().getUid();
        mobileNumber = "1234";
        SimpleDateFormat sdf = new SimpleDateFormat("EEE d-MMM-yyyy GGG HH:mm:ss.SSS z");
        dateAccountCreated = sdf.format(new Date());
        userTimezone = TimeZone.getDefault().getDisplayName();
        userCalender = Calendar.getInstance().getType();
        userLocal = Resources.getSystem().getConfiguration().locale.toString();
        rating = "0";
        isDeleted = Constants.NO;
    }

    private void loadLayout() {

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);
        if (showBackImage){
            imvBack.setVisibility(View.VISIBLE);
        }else {
            imvBack.setVisibility(View.GONE);
        }

        lstOfferedServices = (ListView)findViewById(R.id.lstOfferedServices);

        txvError = (TextView)findViewById(R.id.txvError);

        imvPhoto = (RoundedImageView)findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);

        btnDone = (Button)findViewById(R.id.btnDone);
        btnDone.setClickable(false);
        btnDone.setOnClickListener(this);

        edtFirstName = (EditText)findViewById(R.id.edtFirstName);
        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                firstName = edtFirstName.getText().toString().trim();
                changeButtonStyle_1(btnDone, firstName.length() > 0 && lastName.length() > 0 && email.length() > 0 );
            }
        });

        edtLastName = (EditText)findViewById(R.id.edtLastName);
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                lastName = edtLastName.getText().toString().trim();
                changeButtonStyle_1(btnDone, firstName.length() > 0 && lastName.length() > 0 && email.length() > 0 );
            }
        });

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                changeButtonStyle_1(btnDone, firstName.length() > 0 && lastName.length() > 0 && email.length() > 0 );
            }
        });

        btnHide = (Button) findViewById(R.id.btnHide);
        btnHide.setOnClickListener(this);
        if (selectedBarberId.length() > 0){
            btnHide.setVisibility(View.VISIBLE);
        }else {
            btnHide.setVisibility(View.GONE);
        }
    }

    private void selectPicture() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();
                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        profileImageName = saveFile.getName();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(this, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void handleCreatingABarber() {
        showProgress();

        handleDeletingUnSelectedBarberServiceLink();

        long millis = System.currentTimeMillis(); millis /= 1000;
        String secondaryAppName = String.valueOf(millis);

        // Manually configure Firebase Options
        options = new FirebaseOptions.Builder()
                .setApplicationId("1:1092858221977:android:e65a747df478de36") // Required for Analytics.
                .setApiKey("AIzaSyDOyB5d0oxHT7dE37f3IFHBJVrE52VEQbE") // Required for Auth.
                .setDatabaseUrl("https://cuttiin-f6186.firebaseio.com") // Required for RTDB.
                .build();

        FirebaseApp.initializeApp(this /* Context */, options, secondaryAppName);

        secondaryApp = FirebaseApp.getInstance(secondaryAppName);
        secondaryDatabase = FirebaseDatabase.getInstance(secondaryApp);

        secondAuth = new FirebaseAuth(secondaryApp);
        secondAuth.createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        LogUtil.e(Constants.TAG + "Success");

                        newUserId = authResult.getUser().getUid();
                        sendVerifyEmail(authResult.getUser().getUid());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        txvError.setText(e.getMessage());
                        LogUtil.e(Constants.TAG + "Failure");
                        closeProgress();
                    }
                });
    }

    private void handleDeletingUnSelectedBarberServiceLink() {
        String barberID = selectedBarberId;
        if (services.size() > 0){
            int counterStopper = services.size();
            int counterXX = 0;
            for (ServiceModel servIDD : services){
                counterXX ++;
                
            }
        }
    }

    private void sendVerifyEmail(final String uid) {
        secondAuth.getInstance().getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    showToast("Verification email is sent");
                    saveUserPhoto(uid);
                }else {
                    closeProgress();
                    txvError.setText(task.getException().getMessage());
                    changeButtonStyle(btnDone, false);
                }
            }
        });
    }

    private void saveUserPhoto(final String uid){
        photoRef = storage.getReference().child(Constants.PROFILE_IMAGES).child(uid + ".jpeg");
        UploadTask uploadTask = photoRef.putFile(imageCaptureUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                closeProgress();
                if (!task.isSuccessful()) {
                    LogUtil.e(Constants.TAG + "Failed_00");
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                closeProgress();
                if (task.isSuccessful()) {
                    String downloadUri = task.getResult().toString();
                    // save photo url
                    saveBarberInfo(uid, downloadUri);
                } else {
                    // Handle failures
                    showToast("Upload failed");
                    closeProgress();
                }
            }
        });
    }

    private void saveBarberInfo(final String uid , String profileImageUrl) {
        BarberModel mBarberModel = new BarberModel(uid, firstName, lastName, email, role, barberShopId, profileImageUrl,
                mobileNumber, profileImageName, dateAccountCreated, userTimezone, userCalender, userLocal, rating, isDeleted);
        Commons.DATABASE_USER_REF.child(uid).setValue(mBarberModel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        LogUtil.e(Constants.TAG + "UID == " + uid);
                        LogUtil.e(Constants.TAG + "BarberShopID == " + barberShopId);

                        addBarberToBarberShop();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    private void addBarberToBarberShop() {
        BarberShopBarbers mBarberShopBarber = new BarberShopBarbers(newUserId, dateAccountCreated, userTimezone,
                userCalender, userLocal);

        Commons.BARBERSHOP_BARBERS_REF.child(Commons.mUserModel.getUniqueID()).child(Commons.phoneUid).setValue(mBarberShopBarber)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    updateServiceBarbers();
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
    }

    private void updateServiceBarbers() {

        for (final String serviceId : selectedServiceID){
            final ServiceBarberModel mServiceBarber = new ServiceBarberModel(newUserId, dateAccountCreated, userTimezone,
                    userCalender, userLocal);
            Commons.SERVICE_BARBERS_REF.child(serviceId).child(Commons.phoneUid).setValue(mServiceBarber)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            BarberServiceModel mBarberService = new BarberServiceModel(serviceId, dateAccountCreated,
                                    userTimezone, userCalender, userLocal);

                            Commons.BARBER_SERVICE_REF.child(newUserId).child(Commons.phoneUid).setValue(mBarberService)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            LogUtil.e(Constants.TAG + newUserId);
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
        }

        if (!showBackImage){
            startActivity(new Intent(AddBarberActivity.this, MainBarberActivity.class)); finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        selectedServiceID.clear();
        newUserId = "";

        serviceBarberArrayForLink.clear();
        barberServiceArrayForLink.clear();

        if (selectedBarberId.length() > 0){
            getBarberDataImmutable();
        }

        getServices();
    }

    private void getBarberDataImmutable() {
        String barberId = selectedBarberId;
        Commons.DATABASE_USER_REF.child(barberId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        UserModel mUser = dataSnapshot.getValue(UserModel.class);
                        String firstName = mUser.getFirstName(),
                                lastName = mUser.getLastName(),
                                userEmail = mUser.getEmail(),
                                profileImageURL = mUser.getProfileImageUrl(),
                                checkStatus = mUser.getIsDeleted();

                        edtFirstName.setText(firstName); edtFirstName.setEnabled(false);
                        edtLastName.setText(lastName); edtLastName.setEnabled(false);
                        edtEmail.setText(userEmail); edtEmail.setEnabled(false);

                        if (!profileImageURL.equals(Constants.NIL) && profileImageURL.length() > 0){
                            Picasso.get().load(profileImageURL).into(imvPhoto);
                            imvPhoto.setClickable(false);
                        }

                        if (checkStatus.equals(Constants.YES)){
                            btnDone.setTextColor(getResources().getColor(R.color.white));
                            changeButtonStyle_1(btnDone, true);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private void getServices() {
        Commons.SERVICE_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        ArrayList<ServiceModel> mServiceModelList = new ArrayList<>();

                        for (DataSnapshot postSnapshot :  dataSnapshot.getChildren()){

                            ServiceModel mServiceModel = postSnapshot.getValue(ServiceModel.class);
                            String serviceCheck = mServiceModel.getIsDeleted(),
                                    serviceIDD = mServiceModel.getServiceID();

                            if (serviceCheck.equals(Constants.NO)){
                                services.add(mServiceModel);
                                handleGettingBarberService(serviceIDD);
                                handleGettingServiceBarber(serviceIDD);
                            }

                            mServiceModelList.add(mServiceModel);

                        }

                        mOfferedServicesAdapter = new OfferedServicesAdapter();
                        lstOfferedServices.setAdapter(mOfferedServicesAdapter);
                        mOfferedServicesAdapter.refresh(mServiceModelList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void handleGettingServiceBarber(final String serviceIDDAX) {
        String barberID = selectedBarberId;
        Commons.BARBER_SERVICE_REF.child(barberID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                            BarberServiceModel mBarberService = postSnapshot.getValue(BarberServiceModel.class);
                            String serviceUniqueID = mBarberService.getServiceID();

                            if (serviceUniqueID.equals(serviceIDDAX)){
                                barberServiceArrayForLink.add(mBarberService);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private void handleGettingBarberService(String serviceIDD) {
        Commons.SERVICE_BARBERS_REF.child(serviceIDD)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot :  dataSnapshot.getChildren()){

                            ServiceBarberModel mServiceBarber = postSnapshot.getValue(ServiceBarberModel.class);
                            String barberID = mServiceBarber.getBarberID(),
                                    barberIDAXZ =  selectedBarberId;
                            if (barberID.equals(barberIDAXZ)){
                                serviceBarberArrayForLink.add(mServiceBarber);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private void handleUpdatingBarberServiceLink() {
        String barberID = selectedBarberId;
        if (selectedServiceID.size() > 0){
            handleDeletingUnSelectedBarberServiceLink();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvPhoto:
                selectPicture();
                break;
            case R.id.btnDone:
                if (selectedBarberId.length() == 0){
                    handleCreatingABarber();
                }else {
                    handleUpdatingBarberServiceLink();
                }

                break;
            case R.id.imvBack:
                finish();
                break;
        }
    }

    private class OfferedServicesAdapter extends BaseAdapter {

        ArrayList<ServiceModel> allData = new ArrayList<>();

        public OfferedServicesAdapter() { }

        public void refresh(ArrayList<ServiceModel> data){
            allData.clear();
            allData.addAll(data);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return allData.size();
        }

        @Override
        public ServiceModel getItem(int position) {
            return allData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.item_week, parent, false);
            TextView txvWeekName = (TextView)convertView.findViewById(R.id.txvWeekName);
            txvWeekName.setText(allData.get(position).getServiceTitle());
            CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.chkWeek);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        selectedServiceID.add(allData.get(position).getServiceID());
                    }else {
                        for (int i = 0; i < selectedServiceID.size(); i++){
                            if (selectedServiceID.get(i).equals(selectedServiceID.get(position))){
                                selectedServiceID.remove(i);
                            }
                        }
                    }
                }
            });

            return convertView;
        }
    }
}
