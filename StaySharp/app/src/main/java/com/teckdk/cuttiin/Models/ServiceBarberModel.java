package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class ServiceBarberModel implements Serializable {

    String barberID = "", dateCreated = "", timezone = "", calendar = "", local = "";
    public ServiceBarberModel() { }

    public ServiceBarberModel(String barberID, String dateCreated, String timezone, String calendar, String local) {
        this.barberID = barberID;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getBarberID() {
        return barberID;
    }

    public void setBarberID(String barberID) {
        this.barberID = barberID;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

}
