package com.teckdk.cuttiin.Main;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Fragment.BarberShopFragment;
import com.teckdk.cuttiin.Fragment.BookFragment;
import com.teckdk.cuttiin.Fragment.EditProfileFragment;
import com.teckdk.cuttiin.Fragment.CustomerProfileFragment;
import com.teckdk.cuttiin.Fragment.ResetPasswordFragment;
import com.teckdk.cuttiin.Fragment.SearchFragment;
import com.teckdk.cuttiin.Fragment.SettingFragment;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;

public class    MainCustomActivity extends BaseActivity {

    BarberShopFragment mBarberShopFragment;
    BookFragment mBookFragment;
    CustomerProfileFragment mCustomerProfileFragment;
    EditProfileFragment  mEditProfileFragment;
    SettingFragment mSettingFragment;
    ResetPasswordFragment  mResetPasswordFragment;
    SearchFragment mSearchFragment;

    ImageView imvBooking, imvBarberShop, imvProfile;
    TextView txvBooking, txvBarberShop, txvProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customer);

        loadLayout();
    }

    private void loadLayout() {

        imvBooking =  (ImageView)findViewById(R.id.imvBooking);
        imvBooking.setOnClickListener(this);

        imvBarberShop = (ImageView)findViewById(R.id.imvBarberShop);
        imvBarberShop.setOnClickListener(this);

        imvProfile = (ImageView)findViewById(R.id.imvProfile);
        imvProfile.setOnClickListener(this);

        txvBooking = (TextView)findViewById(R.id.txvBooking);
        txvBarberShop = (TextView)findViewById(R.id.txvBarberShop);
        txvProfile = (TextView)findViewById(R.id.txvProfile);

        showBarberShopFragment();
    }

    public void showBarberShopFragment() {
        initTabIcons();
        imvBarberShop.setImageResource(R.drawable.barbers_active);
        txvBarberShop.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mBarberShopFragment = new BarberShopFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mBarberShopFragment);
        fragmentTransaction.commit();
    }

    private void showBookFragment() {
        initTabIcons();
        imvBooking.setImageResource(R.drawable.bookmark_active);
        txvBooking.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mBookFragment = new BookFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mBookFragment);
        fragmentTransaction.commit();
    }

    public void showProfileFragment() {
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mCustomerProfileFragment = new CustomerProfileFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mCustomerProfileFragment);
        fragmentTransaction.commit();
    }


    public void showEditProfileFragment() {
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mEditProfileFragment = new EditProfileFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mEditProfileFragment);
        fragmentTransaction.commit();
    }

    public void showSettingFragment() {

        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mSettingFragment = new SettingFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mSettingFragment);
        fragmentTransaction.commit();
    }

    public void showResetPasswordFragment(){
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mResetPasswordFragment = new ResetPasswordFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mResetPasswordFragment);
        fragmentTransaction.commit();
    }

    public void showSearchFragment() {
        initTabIcons();
        imvBarberShop.setImageResource(R.drawable.barbers_active);
        txvBarberShop.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mSearchFragment = new SearchFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mSearchFragment);
        fragmentTransaction.commit();
    }

    private void initTabIcons(){
        imvBooking.setImageResource(R.drawable.bookmark_unactive);
        imvBarberShop.setImageResource(R.drawable.barbers_unactive);
        imvProfile.setImageResource(R.drawable.profile_unactive);

        txvBooking.setTextColor(getResources().getColor(R.color.white));
        txvBarberShop.setTextColor(getResources().getColor(R.color.white));
        txvProfile.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBooking:
                showBookFragment();
                break;
            case R.id.imvBarberShop:
                showBarberShopFragment();
                break;
            case R.id.imvProfile:
                showProfileFragment();
                break;
        }
    }
}
