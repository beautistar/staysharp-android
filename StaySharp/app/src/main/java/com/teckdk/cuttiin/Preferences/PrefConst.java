package com.teckdk.cuttiin.Preferences;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_TOKEN = "token";
    public static final String BADGE_CNT= "badgeCnt";

}
