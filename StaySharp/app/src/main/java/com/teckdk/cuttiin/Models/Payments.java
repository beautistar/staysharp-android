package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class Payments implements Serializable {

    String appDateCreated = "", appTimezone = "", appCalendar = "", appLocale = "", barberShopUUIDD = "", barberUUIDD = ""
            , bookingUUIDD = "", customerUUID = "", formattedDateString = "", paymentID = "", dateCreated = "", serviceUUIDD = ""
            , priceTotal = "", paymentTypeOperation = "", payment_qp_status_msg = "", payment_aq_status_msg = "";

    String captureAppDate = "", captureAppTimeZone = "", captureAppCalendar = "", captureAppLocal = "", paymentTypeCaptureOperation = ""
            , paymentCaptureDate = "", capture_qp_status_msg = "", capture_aq_status_msg = "";

    String cancelAppDate = "", cancelAppTimeZone = "", cancelAppCalendar = "", cancelAppLocal = "", paymentTypeCancelOperation = ""
            , paymentCancelDate = "", cancel_qp_status_msg = "", cancel_aq_status_msg = "";

    String refundAppDate = "", refundAppTimeZone = "", refundAppCalendar = "", refundAppLocal = "", paymentTypeRefundOperation = ""
            , paymentRefundDate = "", refund_qp_status_msg = "", refund_aq_status_msg = "", amountRefunded = "";

    String notificationSent = "", methodOfPayment = "";

    public Payments() { }

    public Payments(String appDateCreated, String appTimezone, String appCalendar, String appLocale, String barberShopUUIDD, String barberUUIDD, String bookingUUIDD, String customerUUID, String formattedDateString, String paymentID, String dateCreated, String serviceUUIDD, String priceTotal, String paymentTypeOperation, String payment_qp_status_msg, String payment_aq_status_msg, String captureAppDate, String captureAppTimeZone, String captureAppCalendar, String captureAppLocal, String paymentTypeCaptureOperation, String paymentCaptureDate, String capture_qp_status_msg, String capture_aq_status_msg, String cancelAppDate, String cancelAppTimeZone, String cancelAppCalendar, String cancelAppLocal, String paymentTypeCancelOperation, String paymentCancelDate, String cancel_qp_status_msg, String cancel_aq_status_msg, String refundAppDate, String refundAppTimeZone, String refundAppCalendar, String refundAppLocal, String paymentTypeRefundOperation, String paymentRefundDate, String refund_qp_status_msg, String refund_aq_status_msg, String amountRefunded, String notificationSent, String methodOfPayment) {
        this.appDateCreated = appDateCreated;
        this.appTimezone = appTimezone;
        this.appCalendar = appCalendar;
        this.appLocale = appLocale;
        this.barberShopUUIDD = barberShopUUIDD;
        this.barberUUIDD = barberUUIDD;
        this.bookingUUIDD = bookingUUIDD;
        this.customerUUID = customerUUID;
        this.formattedDateString = formattedDateString;
        this.paymentID = paymentID;
        this.dateCreated = dateCreated;
        this.serviceUUIDD = serviceUUIDD;
        this.priceTotal = priceTotal;
        this.paymentTypeOperation = paymentTypeOperation;
        this.payment_qp_status_msg = payment_qp_status_msg;
        this.payment_aq_status_msg = payment_aq_status_msg;
        this.captureAppDate = captureAppDate;
        this.captureAppTimeZone = captureAppTimeZone;
        this.captureAppCalendar = captureAppCalendar;
        this.captureAppLocal = captureAppLocal;
        this.paymentTypeCaptureOperation = paymentTypeCaptureOperation;
        this.paymentCaptureDate = paymentCaptureDate;
        this.capture_qp_status_msg = capture_qp_status_msg;
        this.capture_aq_status_msg = capture_aq_status_msg;
        this.cancelAppDate = cancelAppDate;
        this.cancelAppTimeZone = cancelAppTimeZone;
        this.cancelAppCalendar = cancelAppCalendar;
        this.cancelAppLocal = cancelAppLocal;
        this.paymentTypeCancelOperation = paymentTypeCancelOperation;
        this.paymentCancelDate = paymentCancelDate;
        this.cancel_qp_status_msg = cancel_qp_status_msg;
        this.cancel_aq_status_msg = cancel_aq_status_msg;
        this.refundAppDate = refundAppDate;
        this.refundAppTimeZone = refundAppTimeZone;
        this.refundAppCalendar = refundAppCalendar;
        this.refundAppLocal = refundAppLocal;
        this.paymentTypeRefundOperation = paymentTypeRefundOperation;
        this.paymentRefundDate = paymentRefundDate;
        this.refund_qp_status_msg = refund_qp_status_msg;
        this.refund_aq_status_msg = refund_aq_status_msg;
        this.amountRefunded = amountRefunded;
        this.notificationSent = notificationSent;
        this.methodOfPayment = methodOfPayment;
    }

    public String getAppDateCreated() {
        return appDateCreated;
    }

    public void setAppDateCreated(String appDateCreated) {
        this.appDateCreated = appDateCreated;
    }

    public String getAppTimezone() {
        return appTimezone;
    }

    public void setAppTimezone(String appTimezone) {
        this.appTimezone = appTimezone;
    }

    public String getAppCalendar() {
        return appCalendar;
    }

    public void setAppCalendar(String appCalendar) {
        this.appCalendar = appCalendar;
    }

    public String getAppLocale() {
        return appLocale;
    }

    public void setAppLocale(String appLocale) {
        this.appLocale = appLocale;
    }

    public String getBarberShopUUIDD() {
        return barberShopUUIDD;
    }

    public void setBarberShopUUIDD(String barberShopUUIDD) {
        this.barberShopUUIDD = barberShopUUIDD;
    }

    public String getBarberUUIDD() {
        return barberUUIDD;
    }

    public void setBarberUUIDD(String barberUUIDD) {
        this.barberUUIDD = barberUUIDD;
    }

    public String getBookingUUIDD() {
        return bookingUUIDD;
    }

    public void setBookingUUIDD(String bookingUUIDD) {
        this.bookingUUIDD = bookingUUIDD;
    }

    public String getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(String customerUUID) {
        this.customerUUID = customerUUID;
    }

    public String getFormattedDateString() {
        return formattedDateString;
    }

    public void setFormattedDateString(String formattedDateString) {
        this.formattedDateString = formattedDateString;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getServiceUUIDD() {
        return serviceUUIDD;
    }

    public void setServiceUUIDD(String serviceUUIDD) {
        this.serviceUUIDD = serviceUUIDD;
    }

    public String getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(String priceTotal) {
        this.priceTotal = priceTotal;
    }

    public String getPaymentTypeOperation() {
        return paymentTypeOperation;
    }

    public void setPaymentTypeOperation(String paymentTypeOperation) {
        this.paymentTypeOperation = paymentTypeOperation;
    }

    public String getPayment_qp_status_msg() {
        return payment_qp_status_msg;
    }

    public void setPayment_qp_status_msg(String payment_qp_status_msg) {
        this.payment_qp_status_msg = payment_qp_status_msg;
    }

    public String getPayment_aq_status_msg() {
        return payment_aq_status_msg;
    }

    public void setPayment_aq_status_msg(String payment_aq_status_msg) {
        this.payment_aq_status_msg = payment_aq_status_msg;
    }

    public String getCaptureAppDate() {
        return captureAppDate;
    }

    public void setCaptureAppDate(String captureAppDate) {
        this.captureAppDate = captureAppDate;
    }

    public String getCaptureAppTimeZone() {
        return captureAppTimeZone;
    }

    public void setCaptureAppTimeZone(String captureAppTimeZone) {
        this.captureAppTimeZone = captureAppTimeZone;
    }

    public String getCaptureAppCalendar() {
        return captureAppCalendar;
    }

    public void setCaptureAppCalendar(String captureAppCalendar) {
        this.captureAppCalendar = captureAppCalendar;
    }

    public String getCaptureAppLocal() {
        return captureAppLocal;
    }

    public void setCaptureAppLocal(String captureAppLocal) {
        this.captureAppLocal = captureAppLocal;
    }

    public String getPaymentTypeCaptureOperation() {
        return paymentTypeCaptureOperation;
    }

    public void setPaymentTypeCaptureOperation(String paymentTypeCaptureOperation) {
        this.paymentTypeCaptureOperation = paymentTypeCaptureOperation;
    }

    public String getPaymentCaptureDate() {
        return paymentCaptureDate;
    }

    public void setPaymentCaptureDate(String paymentCaptureDate) {
        this.paymentCaptureDate = paymentCaptureDate;
    }

    public String getCapture_qp_status_msg() {
        return capture_qp_status_msg;
    }

    public void setCapture_qp_status_msg(String capture_qp_status_msg) {
        this.capture_qp_status_msg = capture_qp_status_msg;
    }

    public String getCapture_aq_status_msg() {
        return capture_aq_status_msg;
    }

    public void setCapture_aq_status_msg(String capture_aq_status_msg) {
        this.capture_aq_status_msg = capture_aq_status_msg;
    }

    public String getCancelAppDate() {
        return cancelAppDate;
    }

    public void setCancelAppDate(String cancelAppDate) {
        this.cancelAppDate = cancelAppDate;
    }

    public String getCancelAppTimeZone() {
        return cancelAppTimeZone;
    }

    public void setCancelAppTimeZone(String cancelAppTimeZone) {
        this.cancelAppTimeZone = cancelAppTimeZone;
    }

    public String getCancelAppCalendar() {
        return cancelAppCalendar;
    }

    public void setCancelAppCalendar(String cancelAppCalendar) {
        this.cancelAppCalendar = cancelAppCalendar;
    }

    public String getCancelAppLocal() {
        return cancelAppLocal;
    }

    public void setCancelAppLocal(String cancelAppLocal) {
        this.cancelAppLocal = cancelAppLocal;
    }

    public String getPaymentTypeCancelOperation() {
        return paymentTypeCancelOperation;
    }

    public void setPaymentTypeCancelOperation(String paymentTypeCancelOperation) {
        this.paymentTypeCancelOperation = paymentTypeCancelOperation;
    }

    public String getPaymentCancelDate() {
        return paymentCancelDate;
    }

    public void setPaymentCancelDate(String paymentCancelDate) {
        this.paymentCancelDate = paymentCancelDate;
    }

    public String getCancel_qp_status_msg() {
        return cancel_qp_status_msg;
    }

    public void setCancel_qp_status_msg(String cancel_qp_status_msg) {
        this.cancel_qp_status_msg = cancel_qp_status_msg;
    }

    public String getCancel_aq_status_msg() {
        return cancel_aq_status_msg;
    }

    public void setCancel_aq_status_msg(String cancel_aq_status_msg) {
        this.cancel_aq_status_msg = cancel_aq_status_msg;
    }

    public String getRefundAppDate() {
        return refundAppDate;
    }

    public void setRefundAppDate(String refundAppDate) {
        this.refundAppDate = refundAppDate;
    }

    public String getRefundAppTimeZone() {
        return refundAppTimeZone;
    }

    public void setRefundAppTimeZone(String refundAppTimeZone) {
        this.refundAppTimeZone = refundAppTimeZone;
    }

    public String getRefundAppCalendar() {
        return refundAppCalendar;
    }

    public void setRefundAppCalendar(String refundAppCalendar) {
        this.refundAppCalendar = refundAppCalendar;
    }

    public String getRefundAppLocal() {
        return refundAppLocal;
    }

    public void setRefundAppLocal(String refundAppLocal) {
        this.refundAppLocal = refundAppLocal;
    }

    public String getPaymentTypeRefundOperation() {
        return paymentTypeRefundOperation;
    }

    public void setPaymentTypeRefundOperation(String paymentTypeRefundOperation) {
        this.paymentTypeRefundOperation = paymentTypeRefundOperation;
    }

    public String getPaymentRefundDate() {
        return paymentRefundDate;
    }

    public void setPaymentRefundDate(String paymentRefundDate) {
        this.paymentRefundDate = paymentRefundDate;
    }

    public String getRefund_qp_status_msg() {
        return refund_qp_status_msg;
    }

    public void setRefund_qp_status_msg(String refund_qp_status_msg) {
        this.refund_qp_status_msg = refund_qp_status_msg;
    }

    public String getRefund_aq_status_msg() {
        return refund_aq_status_msg;
    }

    public void setRefund_aq_status_msg(String refund_aq_status_msg) {
        this.refund_aq_status_msg = refund_aq_status_msg;
    }

    public String getAmountRefunded() {
        return amountRefunded;
    }

    public void setAmountRefunded(String amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public String getNotificationSent() {
        return notificationSent;
    }

    public void setNotificationSent(String notificationSent) {
        this.notificationSent = notificationSent;
    }

    public String getMethodOfPayment() {
        return methodOfPayment;
    }

    public void setMethodOfPayment(String methodOfPayment) {
        this.methodOfPayment = methodOfPayment;
    }
}
