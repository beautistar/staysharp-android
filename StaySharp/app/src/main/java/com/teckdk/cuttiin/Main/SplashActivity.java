package com.teckdk.cuttiin.Main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.provider.Settings;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.R;

@SuppressLint("NewApi")
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();

                // goto login activity
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                /*Intent intent = new Intent(SplashActivity.this, MainBarberActivity.class);
                startActivity(intent);
                finish();*/
            }
        }, Constants.SPLASH_TIME);
    }

    private void init() {
        Commons.loggedIn =  false;
        Commons.phoneUid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
