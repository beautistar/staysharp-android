package com.teckdk.cuttiin.Main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.BarberServiceModel;
import com.teckdk.cuttiin.Models.BarberShopBarbers;
import com.teckdk.cuttiin.Models.ServiceBarberModel;
import com.teckdk.cuttiin.Models.ServiceModel;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.BitmapUtils;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import android.icu.util.Calendar;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class AddServiceActivity extends BaseActivity {

    RoundedImageView imvPhoto;
    Button btnNext;
    EditText edtServiceTitle, edtEstimatedTime, edtCost, edtDescription;
    TextView txvLadies, txvGents, txvKids, txvAssociatedService;
    ImageView imvBack;

    FirebaseStorage storage;
    StorageReference photoRef;
    private DatabaseReference mDatabase;
    FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    ListView lstBarberList;

    private Uri imageCaptureUri;
    String photoPath = "", photoUrl = "", serviceImageName = "";
    String serviceTitle = "", estimatedTime = "", serviceCost = "", shortDescription = "", category = ""
            , localCurrency = "", serviceCostLocal = "", dateCreated = "", timezone = "", calendar = ""
            , local = "", isDeleted = "", objectName = "";

    boolean showBackButton = false;
    ArrayList<TempBarberModel> selectedBarberList = new ArrayList<>();

    BarberAdapter mBarberAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);

        init();

        loadLayout();
    }

    @SuppressLint("NewApi")
    private void init() {

        try {
            showBackButton = getIntent().getBooleanExtra(Constants.SHOW_BACK_IMAGE, false);
        }catch (Exception e){}


        category = Constants.GENTS;
        storage = FirebaseStorage.getInstance();
        mAuth =  FirebaseAuth.getInstance();
        currentUser  = mAuth.getCurrentUser();

        Locale current = getResources().getConfiguration().locale;
        localCurrency = Currency.getInstance(current).getCurrencyCode();

        serviceCostLocal = getResources().getConfiguration().locale.getCountry();

        SimpleDateFormat sdf = new SimpleDateFormat("EEE d-MMM-yyyy GGG HH:mm:ss.SSS z");
        dateCreated = sdf.format(new Date());
        timezone = TimeZone.getDefault().getDisplayName();
        calendar = Calendar.getInstance().getType();
        local = Resources.getSystem().getConfiguration().locale.toString();
        isDeleted = Constants.NO;

        objectName = Commons.phoneUid;
    }

    private void loadLayout() {

        btnNext =  (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        imvPhoto = (RoundedImageView)findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);

        edtServiceTitle = (EditText)findViewById(R.id.edtServiceTitle);
        edtServiceTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                serviceTitle =  edtServiceTitle.getText().toString().trim();
                changeButtonStyle_1(btnNext, serviceTitle.length() > 0 && estimatedTime.length()> 0
                        && serviceCost.length()> 0 && shortDescription.length() > 0 && category.length() > 0);
            }
        });

        edtEstimatedTime = (EditText)findViewById(R.id.edtEstimatedTime);
        edtEstimatedTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                estimatedTime = edtEstimatedTime.getText().toString().trim();
                changeButtonStyle_1(btnNext, serviceTitle.length() > 0 && estimatedTime.length()> 0
                        && serviceCost.length()> 0 && shortDescription.length() > 0 && category.length() > 0);
            }
        });

        edtCost = (EditText)findViewById(R.id.edtCost);
        edtCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                serviceCost =  edtCost.getText().toString().trim();
                changeButtonStyle_1(btnNext, serviceTitle.length() > 0 && estimatedTime.length()> 0
                        && serviceCost.length()> 0 && shortDescription.length() > 0 && category.length() > 0);
            }
        });

        txvLadies = (TextView)findViewById(R.id.txvLadies); txvLadies.setOnClickListener(this);
        txvGents = (TextView)findViewById(R.id.txvGents); txvGents.setOnClickListener(this);
        txvKids = (TextView)findViewById(R.id.txvKids); txvKids.setOnClickListener(this);

        edtDescription = (EditText)findViewById(R.id.edtDescription);
        edtDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                shortDescription = edtDescription.getText().toString().trim();
                changeButtonStyle_1(btnNext, serviceTitle.length() > 0 && estimatedTime.length()> 0
                        && serviceCost.length()> 0 && shortDescription.length() > 0 && category.length() > 0);
            }
        });

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);
        if (showBackButton){
            imvBack.setVisibility(View.VISIBLE);
        }else {
            imvBack.setVisibility(View.GONE);
        }

        lstBarberList = (ListView)findViewById(R.id.lstBarberList);
        mBarberAdapter = new BarberAdapter();
        lstBarberList.setAdapter(mBarberAdapter);

        txvAssociatedService = (TextView)findViewById(R.id.txvAssociatedService);
    }

    private void selectPicture() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();
                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        serviceImageName = saveFile.getName();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(this, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void processUpdateServiceDetails() {

        showProgress();

        if (photoPath.length() > 0){
            photoRef = storage.getReference().child(Constants.SERVICE_IMAGE).child( currentUser.getUid() + ".jpeg");

            UploadTask uploadTask = photoRef.putFile(imageCaptureUri);
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {

                    if (!task.isSuccessful()) {
                        LogUtil.e(Constants.TAG + "Failed_00");
                        closeProgress();
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return photoRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        String downloadUri = task.getResult().toString();
                        // save photo url
                        updateServiceDetails(downloadUri);
                    } else {
                        // Handle failures
                        closeProgress();
                        showToast("Upload failed");
                    }
                }
            });
        }else {
            updateServiceDetails("");
        }
    }

    private void updateServiceDetails(String serviceImageUrl) {
        String barberShopId = currentUser.getUid();
        String serviceId = objectName;

        ServiceModel mServiceModel = new ServiceModel(serviceId, serviceTitle, estimatedTime
                , serviceCost, serviceCostLocal, shortDescription, category, serviceImageName,  serviceImageUrl
                , dateCreated, timezone, calendar, local, localCurrency, isDeleted);

        Commons.SERVICE_REF.child(currentUser.getUid()).child(serviceId).setValue(mServiceModel)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    closeProgress();
                    updateServiceBarbers();
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    closeProgress();
                    showToast("Add Service Failed");
                }
            });
    }

    private void updateServiceBarbers() {
        for (final TempBarberModel mBarber : selectedBarberList){
            ServiceBarberModel mServiceBarber = new ServiceBarberModel(mBarber.getBarberUniqueId(), dateCreated, timezone,
                    calendar, local);
            Commons.SERVICE_BARBERS_REF.child(Commons.phoneUid).child(Commons.phoneUid).setValue(mServiceBarber)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    BarberServiceModel mBarberService = new BarberServiceModel(Commons.phoneUid, dateCreated,
                            timezone, calendar, local);
                    Commons.BARBER_SERVICE_REF.child(mBarber.getBarberUniqueId()).child(Commons.phoneUid).setValue(mBarberService);
                }
            });
        }

        processNext();
    }

    private void processNext() {
        if (!showBackButton){
            startActivity(new Intent(AddServiceActivity.this, AddBarberActivity.class)); finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        selectedBarberList.clear();
        mBarberAdapter.clearAll();

        getThisServiceAssociatedBarbers();
    }

    private void getThisServiceAssociatedBarbers() {
        Commons.BARBERSHOP_BARBERS_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postDataSnap :  dataSnapshot.getChildren()){
                            final String barberId = (postDataSnap.getValue(BarberShopBarbers.class)).getBarberStaffID();
                            Commons.DATABASE_USER_REF.child(barberId)
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            UserModel mUser = dataSnapshot.getValue(UserModel.class);
                                            String userName = mUser.getFirstName() + " " + mUser.getLastName();

                                            TempBarberModel mTempBarber = new TempBarberModel(barberId, userName);

                                            mBarberAdapter.add(mTempBarber);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNext:
                processUpdateServiceDetails();
                break;
            case R.id.txvLadies:
                category = Constants.LADIES;
                txvLadies.setBackgroundResource(R.drawable.bg_ladies); txvLadies.setTextColor(getResources().getColor(R.color.white));
                txvGents.setBackgroundColor(getResources().getColor(R.color.transparent)); txvGents.setTextColor(getResources().getColor(R.color.mainColor));
                txvKids.setBackgroundColor(getResources().getColor(R.color.transparent)); txvKids.setTextColor(getResources().getColor(R.color.mainColor));
                txvAssociatedService.setText("This will add the service in the " + "Ladies" + " section only");
                break;
            case R.id.txvGents:
                category = Constants.GENTS;
                txvGents.setBackgroundResource(R.drawable.bg_gents); txvGents.setTextColor(getResources().getColor(R.color.white));
                txvLadies.setBackgroundColor(getResources().getColor(R.color.transparent)); txvLadies.setTextColor(getResources().getColor(R.color.mainColor));
                txvKids.setBackgroundColor(getResources().getColor(R.color.transparent)); txvKids.setTextColor(getResources().getColor(R.color.mainColor));
                txvAssociatedService.setText("This will add the service in the " + "Gents" + " section only");
                break;
            case R.id.txvKids:
                category = Constants.KIDS;
                txvKids.setBackgroundResource(R.drawable.bg_kids); txvKids.setTextColor(getResources().getColor(R.color.white));
                txvLadies.setBackgroundColor(getResources().getColor(R.color.transparent)); txvLadies.setTextColor(getResources().getColor(R.color.mainColor));
                txvGents.setBackgroundColor(getResources().getColor(R.color.transparent)); txvGents.setTextColor(getResources().getColor(R.color.mainColor));
                txvAssociatedService.setText("This will add the service in the " + "Kids" + " section only");
                break;
            case R.id.imvPhoto:
                selectPicture();
                break;
            case R.id.imvBack:
                finish();
                break;
        }
    }

    private class BarberAdapter extends BaseAdapter {

        ArrayList<TempBarberModel> allData = new ArrayList<>();

        public void add(TempBarberModel data){
            allData.add(data);
            notifyDataSetChanged();
        }

        public void clearAll(){
            allData.clear();
        }

        @Override
        public int getCount() {
            return allData.size();
        }

        @Override
        public TempBarberModel getItem(int position) {
            return allData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.item_week, parent, false);
            TextView txvWeekName = (TextView)convertView.findViewById(R.id.txvWeekName);
            txvWeekName.setText(allData.get(position).getBarberName());
            CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.chkWeek);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        selectedBarberList.add(allData.get(position));
                    }else {
                        for (int i =  0; i < selectedBarberList.size(); i++){
                            if (selectedBarberList.get(i).getBarberName().equals(allData.get(position).getBarberName())){
                                selectedBarberList.remove(i);
                            }
                        }
                    }
                }
            });
            return convertView;
        }
    }

    private class TempBarberModel{
        String barberUniqueId = "", barberName = "";

        public TempBarberModel(String barberUniqueId, String barberName) {
            this.barberUniqueId = barberUniqueId;
            this.barberName = barberName;
        }

        public String getBarberUniqueId() {
            return barberUniqueId;
        }

        public void setBarberUniqueId(String barberUniqueId) {
            this.barberUniqueId = barberUniqueId;
        }

        public String getBarberName() {
            return barberName;
        }

        public void setBarberName(String barberName) {
            this.barberName = barberName;
        }
    }
}
