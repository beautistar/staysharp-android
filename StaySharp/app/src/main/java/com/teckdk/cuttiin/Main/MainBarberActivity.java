package com.teckdk.cuttiin.Main;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Fragment.AppointmentFragment;
import com.teckdk.cuttiin.Fragment.BarberProfileFragment;
import com.teckdk.cuttiin.Fragment.BarberShopProfileSettingsFragment;
import com.teckdk.cuttiin.Fragment.CalendarFragment;
import com.teckdk.cuttiin.Fragment.ChangePasswordForBarberShopFragment;
import com.teckdk.cuttiin.Fragment.EditBarberShopProfileFragment;
import com.teckdk.cuttiin.R;

public class MainBarberActivity extends BaseActivity {

    CalendarFragment  mCalendarFragment;
    AppointmentFragment mAppointmentFragment;
    BarberProfileFragment mBarberProfileFragment;
    EditBarberShopProfileFragment mEditBarberShopProfileFragment;
    BarberShopProfileSettingsFragment mBarberShopProfileSettingsFragment;
    ChangePasswordForBarberShopFragment mChangePasswordForBarberShopFragment;

    ImageView imvCalendar, imvBarberShop, imvProfile;
    TextView txvCalendar, txvBarberShop, txvProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_barber);

        loadLayout();
    }

    private void loadLayout() {
        imvCalendar =  (ImageView)findViewById(R.id.imvCalendar);
        imvCalendar.setOnClickListener(this);

        imvBarberShop =  (ImageView)findViewById(R.id.imvBarberShop);
        imvBarberShop.setOnClickListener(this);

        imvProfile =  (ImageView)findViewById(R.id.imvProfile);
        imvProfile.setOnClickListener(this);

        txvCalendar = (TextView)findViewById(R.id.txvCalendar);
        txvBarberShop = (TextView)findViewById(R.id.txvBarberShop);
        txvProfile = (TextView)findViewById(R.id.txvProfile);

        showCalendarFragment();
    }

    public void showCalendarFragment() {
        initTabIcons();
        imvCalendar.setImageResource(R.drawable.calendar_active);
        txvCalendar.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mCalendarFragment = new CalendarFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mCalendarFragment);
        fragmentTransaction.commit();
    }

    public void showAppointmentFragment(){
        initTabIcons();
        imvBarberShop.setImageResource(R.drawable.barbers_active);
        txvBarberShop.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mAppointmentFragment = new AppointmentFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mAppointmentFragment);
        fragmentTransaction.commit();
    }

    public void showBarberProfileFragment(){
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mBarberProfileFragment = new BarberProfileFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mBarberProfileFragment);
        fragmentTransaction.commit();
    }

    public void showEditBarberProfileFragment(){
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mEditBarberShopProfileFragment = new EditBarberShopProfileFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mEditBarberShopProfileFragment);
        fragmentTransaction.commit();
    }

    public void showBarberShopProfileSettingsFragment(){
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mBarberShopProfileSettingsFragment = new BarberShopProfileSettingsFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mBarberShopProfileSettingsFragment);
        fragmentTransaction.commit();
    }

    public void showChangePasswordForBarberShopFragment(){
        initTabIcons();
        imvProfile.setImageResource(R.drawable.profile_active);
        txvProfile.setTextColor(getResources().getColor(R.color.whiteBlue));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mChangePasswordForBarberShopFragment = new ChangePasswordForBarberShopFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mChangePasswordForBarberShopFragment);
        fragmentTransaction.commit();
    }

    private void initTabIcons() {
        imvCalendar.setImageResource(R.drawable.calendar_unactive);
        txvCalendar.setTextColor(getResources().getColor(R.color.white));

        imvBarberShop.setImageResource(R.drawable.barbers_unactive);
        txvBarberShop.setTextColor(getResources().getColor(R.color.white));

        imvProfile.setImageResource(R.drawable.profile_unactive);
        txvProfile.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvCalendar:
                showCalendarFragment();
                break;
            case R.id.imvBarberShop:
                showAppointmentFragment();
                break;
            case R.id.imvProfile:
                showBarberProfileFragment();
                break;
        }
    }
}
