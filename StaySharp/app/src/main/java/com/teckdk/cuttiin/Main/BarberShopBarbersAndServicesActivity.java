package com.teckdk.cuttiin.Main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.teckdk.cuttiin.Adapter.BarbersAdapter;
import com.teckdk.cuttiin.Adapter.ServiceAdapter;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.BarberShopBarbers;
import com.teckdk.cuttiin.Models.ServiceModel;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.R;

import java.util.ArrayList;

public class BarberShopBarbersAndServicesActivity extends BaseActivity {

    BarbersAdapter mBarbersAdapter;
    ServiceAdapter mServiceAdapter;

    ImageView imvBack;
    TextView txvBarbers, txvServices, txvAddBarberAndService, txvLadies, txvGents, txvKids;
    ListView lstBarberList, lstServiceList;
    LinearLayout lytServiceList, lytAddBarberAndService;
    RelativeLayout rytLadies, rytGents, rytKids;
    View viewLadies, viewGents, viewKids;

    ArrayList<ServiceModel> ladiesServiceList = new ArrayList<>();
    ArrayList<ServiceModel> gentsServiceList =  new ArrayList<>();
    ArrayList<ServiceModel> kidsServiceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barber_shop_barbers_and_services);

        loadLayout();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        txvBarbers = (TextView)findViewById(R.id.txvBarbers);
        txvBarbers.setOnClickListener(this);

        txvServices = (TextView)findViewById(R.id.txvServices);
        txvServices.setOnClickListener(this);

        txvAddBarberAndService = (TextView)findViewById(R.id.txvAddBarberAndService);

        mBarbersAdapter = new BarbersAdapter(this);
        lstBarberList = (ListView)findViewById(R.id.lstBarberList);
        lstBarberList.setAdapter(mBarbersAdapter);
        lstBarberList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String barberUniqueId = mBarbersAdapter.getItem(position).getUniqueID();
                Intent intent = new Intent(BarberShopBarbersAndServicesActivity.this, AddBarberActivity.class);
                intent.putExtra(Constants.BARBER_ID, barberUniqueId);
                intent.putExtra(Constants.SHOW_BACK_IMAGE, true);
                startActivity(intent);
            }
        });

        lstServiceList = (ListView)findViewById(R.id.lstServiceList);

        lytServiceList = (LinearLayout)findViewById(R.id.lytServiceList);

        rytLadies = (RelativeLayout)findViewById(R.id.rytLadies);
        rytLadies.setOnClickListener(this);

        rytGents = (RelativeLayout)findViewById(R.id.rytGents);
        rytGents.setOnClickListener(this);

        rytKids = (RelativeLayout)findViewById(R.id.rytKids);
        rytKids.setOnClickListener(this);

        viewLadies = (View)findViewById(R.id.viewLadies);
        viewGents = (View)findViewById(R.id.viewGents);
        viewKids = (View)findViewById(R.id.viewKids);

        txvLadies = (TextView)findViewById(R.id.txvLadies);
        txvGents = (TextView)findViewById(R.id.txvGents);
        txvKids = (TextView)findViewById(R.id.txvKids);

        lytAddBarberAndService = (LinearLayout)findViewById(R.id.lytAddBarberAndService);
        lytAddBarberAndService.setOnClickListener(this);
    }

    private void showBarberList(){
        txvBarbers.setBackground(getResources().getDrawable(R.drawable.bg_left_cornor_round_blue));
        txvBarbers.setTextColor(getResources().getColor(R.color.white));

        txvServices.setBackgroundColor(getResources().getColor(R.color.transparent));
        txvServices.setTextColor(getResources().getColor(R.color.blackColor));

        txvAddBarberAndService.setText(Constants.ADD_BARBER);

        lstBarberList.setVisibility(View.VISIBLE);
        lytServiceList.setVisibility(View.GONE);
    }

    private void showServiceList(){
        txvServices.setBackground(getResources().getDrawable(R.drawable.bg_right_cornor_round_blue));
        txvServices.setTextColor(getResources().getColor(R.color.white));

        txvBarbers.setBackgroundColor(getResources().getColor(R.color.transparent));
        txvBarbers.setTextColor(getResources().getColor(R.color.blackColor));

        txvAddBarberAndService.setText(Constants.ADD_SERVICE);

        lytServiceList.setVisibility(View.VISIBLE);
        lstBarberList.setVisibility(View.GONE);
    }

    private void showLadiesServiceList() {
        viewLadies.setVisibility(View.VISIBLE);
        viewGents.setVisibility(View.INVISIBLE);
        viewKids.setVisibility(View.INVISIBLE);

        mServiceAdapter = new ServiceAdapter(this);
        lstServiceList.setAdapter(mServiceAdapter);
        mServiceAdapter.refresh(ladiesServiceList);
    }

    private void showGentsServiceList() {
        viewGents.setVisibility(View.VISIBLE);
        viewLadies.setVisibility(View.INVISIBLE);
        viewKids.setVisibility(View.INVISIBLE);

        mServiceAdapter = new ServiceAdapter(this);
        lstServiceList.setAdapter(mServiceAdapter);
        mServiceAdapter.refresh(gentsServiceList);
    }

    private void showKidsServiceList() {
        viewKids.setVisibility(View.VISIBLE);
        viewLadies.setVisibility(View.INVISIBLE);
        viewGents.setVisibility(View.INVISIBLE);

        mServiceAdapter = new ServiceAdapter(this);
        lstServiceList.setAdapter(mServiceAdapter);
        mServiceAdapter.refresh(kidsServiceList);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBarbersAdapter.clearAll();
        kidsServiceList.clear();
        ladiesServiceList.clear();
        gentsServiceList.clear();

        getThisBarberShopBarbers();
        getBarberShopServices();
    }

    private void getBarberShopServices() {
        Commons.SERVICE_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                            ServiceModel mServiceModel = postSnapshot.getValue(ServiceModel.class);
                            switch (mServiceModel.getCategory()){
                                case Constants.KIDS:
                                    kidsServiceList.add(mServiceModel);
                                    break;
                                case Constants.LADIES:
                                    ladiesServiceList.add(mServiceModel);
                                    break;
                                case Constants.GENTS:
                                    gentsServiceList.add(mServiceModel);
                                    break;
                            }
                        }

                        mServiceAdapter = new ServiceAdapter(BarberShopBarbersAndServicesActivity.this);
                        lstServiceList.setAdapter(mServiceAdapter);
                        mServiceAdapter.refresh(gentsServiceList);

                        txvLadies.setText("LADIES(" + ladiesServiceList.size() + ")");
                        txvGents.setText("GENTS(" + gentsServiceList.size() + ")");
                        txvKids.setText("KIDS(" + kidsServiceList.size() + ")");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getThisBarberShopBarbers() {
        showProgress();
        Commons.BARBERSHOP_BARBERS_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot postSnapshot  : dataSnapshot.getChildren()){
                            BarberShopBarbers mBarberShopBarbers =  postSnapshot.getValue(BarberShopBarbers.class);

                            getListOfBarbers(mBarberShopBarbers.getBarberStaffID());
                        }

                        closeProgress();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getListOfBarbers(String barberId) {
        Commons.DATABASE_USER_REF.child(barberId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mBarbersAdapter.add( dataSnapshot.getValue(UserModel.class));
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
            case R.id.txvBarbers:
                showBarberList();
                break;
            case R.id.txvServices:
                showServiceList();
                break;
            case R.id.rytLadies:
                showLadiesServiceList();
                break;
            case R.id.rytGents:
                showGentsServiceList();
                break;
            case R.id.rytKids:
                showKidsServiceList();
                break;
            case R.id.lytAddBarberAndService:
                if (txvAddBarberAndService.getText().toString().trim().equals(Constants.ADD_BARBER)){
                    Intent intent = new Intent(this, AddBarberActivity.class);
                    intent.putExtra(Constants.SHOW_BACK_IMAGE, true);
                    startActivity(intent);
                }else if (txvAddBarberAndService.getText().toString().trim().equals(Constants.ADD_SERVICE)){
                    Intent intent = new Intent(this, AddServiceActivity.class);
                    intent.putExtra(Constants.SHOW_BACK_IMAGE, true);
                    startActivity(intent);
                }
                break;
        }
    }
}
