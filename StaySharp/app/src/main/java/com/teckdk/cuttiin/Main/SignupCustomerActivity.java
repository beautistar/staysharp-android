package com.teckdk.cuttiin.Main;

import android.accounts.NetworkErrorException;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthEmailException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.CustomerModel;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@SuppressLint("NewApi")
public class SignupCustomerActivity extends BaseActivity{

    EditText edtFirstName, edtLastName, edtEmail, edtPassword;
    TextView txvShowHide, txvError;
    Button btnSignup;
    ImageView imvBack;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    String firstName = "", lastName = "", email = "", password = "";
    String role = "", profileImageUrl = "", mobileNumber = "", profileImageName = ""
            , dateAccountCreated = "", userTimezone = "", userCalender = "", userLocal = ""
            , cardID = "", cardDateCreated = "", cardDateCalendar = "", cardDateTimeZone = ""
            , cardDateLocale = "", cardHasBeenDeleted = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_customer);

        init();
        loadLayout();
    }

    private void init() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        role = Constants.USER_CUSTOMER;
        profileImageUrl = Constants.NIL;
        mobileNumber = Constants.MOBILE_NUMBER;
        profileImageName = Constants.NIL;
        userTimezone = TimeZone.getDefault().getDisplayName();
        userCalender = Calendar.getInstance().getType();
        userLocal = Resources.getSystem().getConfiguration().locale.toString();
        cardID = Constants.NOT_AVAILABLE;
        cardDateCreated = Constants.NOT_AVAILABLE;
        cardDateCalendar = Constants.NOT_AVAILABLE;
        cardDateTimeZone = Constants.NOT_AVAILABLE;
        cardDateLocale = Constants.NOT_AVAILABLE;
        cardHasBeenDeleted = Constants.YES;
    }

    private void loadLayout() {

        txvError = (TextView)findViewById(R.id.txvSignupError);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        btnSignup = (Button)findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);
        btnSignup.setClickable(false);

        edtFirstName =  (EditText)findViewById(R.id.edtFirstName);
        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                firstName =  edtFirstName.getText().toString().trim();
                changeButtonStyle(btnSignup, checkInputParameters());
            }
        });

        edtLastName = (EditText)findViewById(R.id.edtLastName);
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                lastName =  edtLastName.getText().toString().trim();
                changeButtonStyle(btnSignup, checkInputParameters());
            }
        });

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                changeButtonStyle(btnSignup, checkInputParameters());
            }
        });

        edtPassword =  (EditText)findViewById(R.id.edtPassword);
        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                password = edtPassword.getText().toString().trim();
                changeButtonStyle(btnSignup, checkInputParameters());
            }
        });

        txvShowHide =  (TextView)findViewById(R.id.txvShowHide);
        txvShowHide.setOnClickListener(this);
    }

    private boolean checkInputParameters(){
        return (firstName.length() > 0 && lastName.length() > 0 && email.length() > 0 && email.contains("@") && password.length() > 6);
    }

    private void processSingup() {
        firstName = edtFirstName.getText().toString().trim();
        lastName = edtLastName.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();

        if (firstName.length() == 0 && lastName.length() == 0 && email.length() == 0 && password.length() == 0) return;

        showProgress();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        closeProgress();
                        if (task.isSuccessful()){
                            LogUtil.e(Constants.TAG + "Signup success");
                            Preference.getInstance().put(SignupCustomerActivity.this, PrefConst.PREFKEY_USERPWD, password);
                            Commons.password = password;
                            sendVerificationEmail();
                        }else {
                            changeButtonStyle(btnSignup, false);
                            try {
                                throw  task.getException();
                            }catch (FirebaseAuthWeakPasswordException e){
                                txvError.setText(getString(R.string.week_password));
                            }catch (FirebaseAuthInvalidCredentialsException e){
                                txvError.setText(getString(R.string.invalid_email));
                            }catch (FirebaseAuthUserCollisionException e){
                                txvError.setText(getString(R.string.exist_user));
                            }catch (NetworkErrorException e){
                                txvError.setText(getString(R.string.signup_network_error));
                            } catch (Exception e){
                                txvError.setText(getString(R.string.default_error));
                            }
                        }
                    }
                });
    }

    private void sendVerificationEmail() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            showToast("Verification email is sent");
                            saveUserInFirebase(user.getUid());
                        }else {
                            try {
                                throw  task.getException();
                            }catch (NetworkErrorException e){
                                txvError.setText(getString(R.string.signup_network_error));
                            }catch (FirebaseAuthEmailException e){
                                txvError.setText(getString(R.string.account_not_found));
                            } catch (Exception e){
                                txvError.setText(getString(R.string.default_error));
                            }

                            changeButtonStyle(btnSignup, false);
                        }
                    }
                });
    }

    private void saveUserInFirebase(final String uid) {

        SimpleDateFormat sdf = new SimpleDateFormat("EEE d-MMM-yyyy GGG HH:mm:ss.SSS z");
        dateAccountCreated = sdf.format(new Date());

        final CustomerModel mCustomerModel = new CustomerModel(uid, firstName, lastName, email, role,  profileImageUrl, mobileNumber,
                profileImageName, dateAccountCreated, userTimezone, userCalender, userLocal, cardID,
                cardDateCreated, cardDateCalendar, cardDateTimeZone, cardDateLocale, cardHasBeenDeleted);

        mDatabase.child("users").child(uid).setValue(mCustomerModel)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                LogUtil.e(Constants.TAG + "UID == " + uid);

                Preference.getInstance().put(SignupCustomerActivity.this, PrefConst.PREFKEY_USEREMAIL, email);
                Preference.getInstance().put(SignupCustomerActivity.this, PrefConst.PREFKEY_USERPWD, password);

                startActivity(new Intent(SignupCustomerActivity.this, UploadProfilePictureActivity.class)); finish();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                LogUtil.e(Constants.TAG + "User data save failed");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txvShowHide:
                txvShowHide.setSelected(!txvShowHide.isSelected());
                if (txvShowHide.isSelected()){
                    txvShowHide.setText("Hide");
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                }else {
                    txvShowHide.setText("Show");
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            break;
            case R.id.btnSignup:
                txvError.setText("");
                processSingup();
                break;
            case R.id.imvBack:
                startActivity(new Intent(this, LoginActivity.class)); finish();
                break;
        }
    }
}
