package com.teckdk.cuttiin.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.BitmapUtils;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.io.File;
import java.io.InputStream;

public class ChoosePhotoActivity extends BaseActivity {

    EditText edtCompanyName, edtAccountNumerRegNr, edtAccountNumerKontoNr,edtCompanyAddress;
    Button btnNext;
    RoundedImageView imvPhoto;

    FirebaseStorage storage;
    StorageReference photoRef;
    private DatabaseReference mDatabase;

    String companyName = "", companyAddress = "", accountNumerRegNr = "", accountNumerKontoNr = "";
    private Uri imageCaptureUri;
    String photoPath = "", photoUrl = "", companyLogoImageName = "", companyLatitude = "", companyLongitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_photo);

        init();
        loadLayout();
    }

    private void init() {
        storage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    private void loadLayout() {

        imvPhoto = (RoundedImageView)findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);

        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        btnNext.setClickable(false);

        edtCompanyName = (EditText)findViewById(R.id.edtCompanyName);
        edtCompanyName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {

                companyName =  edtCompanyName.getText().toString().trim();
                changeButtonStyle_1(btnNext, companyName.length() > 0 && companyAddress.length() > 0 && accountNumerRegNr.length() > 0 && accountNumerKontoNr.length() > 0);
            }
        });

        edtCompanyAddress = (EditText)findViewById(R.id.edtCompanyAddress);
        edtCompanyAddress.setOnClickListener(this);
        edtCompanyAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                companyAddress = edtCompanyAddress.getText().toString().trim();
                changeButtonStyle_1(btnNext, companyName.length() > 0 && companyAddress.length() > 0 && accountNumerRegNr.length() > 0 && accountNumerKontoNr.length() > 0);
            }
        });

        edtAccountNumerRegNr =  (EditText)findViewById(R.id.edtAccountNumerRegNr);
        edtAccountNumerRegNr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                accountNumerRegNr = edtAccountNumerRegNr.getText().toString().trim();
                changeButtonStyle_1(btnNext, companyName.length() > 0 && companyAddress.length() > 0 && accountNumerRegNr.length() > 0 && accountNumerKontoNr.length() > 0);
            }
        });

        edtAccountNumerKontoNr = (EditText)findViewById(R.id.edtAccountNumerKontoNr);
        edtAccountNumerKontoNr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                accountNumerKontoNr = edtAccountNumerKontoNr.getText().toString().trim();
                changeButtonStyle_1(btnNext, companyName.length() > 0 && companyAddress.length() > 0 && accountNumerRegNr.length() > 0 && accountNumerKontoNr.length() > 0);
            }
        });
    }
    private void selectPicture() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();
                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        companyLogoImageName = saveFile.getName();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA: {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(this, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }

            case Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    edtCompanyAddress.setText(place.getName());
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    // TODO: Handle the error.
//                    Log.i(TAG, status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
                break;

        }
    }

    private void processUpdateBarbershop() {
        if (photoPath.length() > 0){
            showProgress();
            photoRef = storage.getReference().child(Constants.COMPANY_LOGO).child(Commons.mUserModel.getUniqueID() + ".jpeg");

            UploadTask uploadTask = photoRef.putFile(imageCaptureUri);
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    closeProgress();
                    if (!task.isSuccessful()) {
                        LogUtil.e(Constants.TAG + "Failed_00");
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return photoRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        String companyLogoImageUrl = task.getResult().toString();
                        updateBarbershopProfile(companyLogoImageUrl);
                    } else {
                        // Handle failures
                        showToast("Upload failed");
                        closeProgress();
                    }
                }
            });
        }else {
            updateBarbershopProfile(Constants.NIL);
        }
    }

    private void updateBarbershopProfile(String companyLogoImageUrl) {
        closeProgress();

        companyLatitude = String.valueOf(Commons.lat);
        companyLongitude = String.valueOf(Commons.lng);

        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("companyName").setValue(companyName);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("companyFormattedAddress").setValue(companyAddress);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("companyAddressLongitude").setValue(companyLongitude);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("companyAddressLatitude").setValue(companyLatitude);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("accountNumerRegNr").setValue(accountNumerRegNr);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("accountNumerKontoNr").setValue(accountNumerKontoNr);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("companyLogoImageUrl").setValue(companyLogoImageUrl);
        mDatabase.child("users").child(Commons.mBarberModel.getUniqueID()).child("companyLogoImageName").setValue(companyLogoImageName);

        startActivity(new Intent(this, OpeningHoursActivity.class)); finish();
    }

    private void getCompanyAddress() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
            startActivityForResult(intent, Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNext:
                processUpdateBarbershop();
                break;
            case R.id.imvPhoto:
                selectPicture();
                break;
            case R.id.edtCompanyAddress:
                getCompanyAddress();
                break;
        }
    }
}
