package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Main.MainBarberActivity;
import com.teckdk.cuttiin.Main.MainCustomActivity;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class ChangePasswordForBarberShopFragment extends BaseFragment {

    Context context;

    ImageView imvBack;
    EditText edtEmail;
    Button btnSend;
    TextView txvGoToBack;

    String email = "";

    public ChangePasswordForBarberShopFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_reset_password, container, false);

        imvBack = (ImageView)fragment.findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        btnSend = (Button)fragment.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        btnSend.setClickable(false);

        edtEmail = (EditText)fragment.findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                ((MainBarberActivity)context).changeButtonStyle(btnSend, email.contains("@") && email.length() > 0);
            }
        });

        return fragment;
    }

    private void resetPassword(){
        ((MainCustomActivity)context).showProgress();
        FirebaseAuth.getInstance().sendPasswordResetEmail(Commons.mUserModel.getEmail())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        ((MainCustomActivity)context).closeProgress();
                        if (task.isSuccessful()) {
                            ((MainBarberActivity)context).showAlertDialog("Email sent");
                            ((MainBarberActivity)context).showBarberShopProfileSettingsFragment();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                ((MainBarberActivity)context).showBarberShopProfileSettingsFragment();
                break;
            case R.id.btnSend:
                resetPassword();
                break;
        }
    }
}
