package com.teckdk.cuttiin.Main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class FeedbackActivity extends BaseActivity {

    ImageView imvBack;
    Button btnYes, btnNo, btnSubmit;
    LinearLayout lytGiveRate, lytNoRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        loadLayout();
    }

    private void loadLayout() {

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        btnYes = (Button)findViewById(R.id.btnYes);
        btnYes.setOnClickListener(this);

        btnNo = (Button)findViewById(R.id.btnNo);
        btnNo.setOnClickListener(this);

        btnSubmit =  (Button)findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        lytGiveRate = (LinearLayout) findViewById(R.id.lytGiveRate);
        lytNoRate = (LinearLayout)findViewById(R.id.lytNoRate);
    }

    private void showNoRatingLayout() {
        btnSubmit.setVisibility(View.VISIBLE);
        lytNoRate.setVisibility(View.VISIBLE);
        lytGiveRate.setVisibility(View.GONE);
    }

    private void showRattingLayout() {
        btnSubmit.setVisibility(View.VISIBLE);
        lytGiveRate.setVisibility(View.VISIBLE);
        lytNoRate.setVisibility(View.GONE);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnYes:
                showRattingLayout();
                break;
            case R.id.btnNo:
                showNoRatingLayout();
                break;
            case R.id.imvBack:
                finish();
                break;
        }
    }
}
