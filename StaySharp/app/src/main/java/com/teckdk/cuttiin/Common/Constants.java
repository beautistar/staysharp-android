package com.teckdk.cuttiin.Common;

import android.app.Service;

import java.net.PortUnreachableException;

public class Constants {
    public static final String TAG = "StaySharp====";
    public static final int SPLASH_TIME = 2000;
    public static final String FOR_BARBERS = "FOR BARBERS!";
    public static final String CUSTOMER = "GO BACK!";
    public static final int LOCATION_UPDATE_TIME = 1000;
    public static final int MY_PEQUEST_CODE = 104;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final String USER_CUSTOMER = "customer";
    public static final String USER_BARBER_SHOP = "barberShop";
    public static final String USER_BARBER_STAFF = "barberStaff";
    public static final int PROFILE_IMAGE_SIZE = 256;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final String NOT_AVAILABLE = "not available";
    public static final String NIL = "nil";
    public static final String YES = "YES";
    public static final String NO = "NO";
    public static final String MOBILE_NUMBER = "1234";

    public static final String PROFILE_IMAGES = "profileImages";
    public static final String COMPANY_LOGO = "companyLogo";
    public static final String SELECTED_DAY = "selectedDay";
    public static final String SERVICE_IMAGE = "serviceImage";

    public static final String MON = "Monday", TUE = "Tuesday", WED = "Wednesday", THU = "Thursday", FRI = "Friday", SAT = "Saturday", SUN = "Sunday";
    public static final String[] DAYS = {MON, TUE, WED, THU, FRI, SAT, SUN};
    public static final String LADIES ="Ladies", GENTS = "Gents", KIDS = "Kids";

    public static final String PASSWORD = "theCuttingAppPassKey123";
    public static final String BARBER_STAFF = "barberStaff";

    public static final String ADD_BARBER = "ADD BARBER";
    public static final String ADD_SERVICE = "ADD SERVICE";
    public static final String SHOW_BACK_IMAGE = "ShowBackImage";
    public static final String APPROVED = "Approved";
    public static final String BARBER_ID = "barberId";
}
