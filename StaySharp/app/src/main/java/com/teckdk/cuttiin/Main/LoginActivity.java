package com.teckdk.cuttiin.Main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Service.GpsService;

public class LoginActivity extends BaseActivity {

    Button btnLogin, btnCreateAccount, btnFacebook;
    TextView txvUserType;

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS, android.Manifest.permission.CAMERA, android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadLayout();
        checkAllPermission();
        initData();
    }

    private void loadLayout() {
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        btnCreateAccount = (Button)findViewById(R.id.btnCreateAccount);
        btnCreateAccount.setOnClickListener(this);

        txvUserType = (TextView)findViewById(R.id.txvUserType);
        txvUserType.setOnClickListener(this);

        btnFacebook = (Button)findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(this);
    }

    private void initData() {
        Commons.userType = Constants.USER_CUSTOMER;
    }

    private void gotoForBarbers() {
        btnFacebook.setVisibility(View.INVISIBLE);
        btnCreateAccount.setText("REGISTER");
        txvUserType.setText(Constants.CUSTOMER);
        Commons.userType = Constants.USER_BARBER_SHOP;
    }

    private void gotoForCustomer() {
        btnFacebook.setVisibility(View.VISIBLE);
        btnCreateAccount.setText("CREATE AN ACCOUNT");
        txvUserType.setText(Constants.FOR_BARBERS);

        Commons.userType = Constants.USER_CUSTOMER;
    }

    //==================== Permission========================================
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) { }
        runGpsService();
    }
    //==================================================================

    private void runGpsService(){
        Intent serviceIntent = new Intent(this, GpsService.class);
        startService(serviceIntent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                startActivity(new Intent(this, LoginWithEmailActivity.class)); finish();
                break;
            case R.id.btnCreateAccount:
                String temp1  = txvUserType.getText().toString().trim();
                if (temp1.equals(Constants.FOR_BARBERS)){
                    startActivity(new Intent(this, SignupCustomerActivity.class)); finish();
                }else if (temp1.equals(Constants.CUSTOMER)){
                    startActivity(new Intent(this, RegisterBarbersActivity.class)); finish();
                }
                break;
            case R.id.txvUserType:
                String temp =  txvUserType.getText().toString().trim();
                if (temp.equals(Constants.FOR_BARBERS)){
                    gotoForBarbers();
                }else if (temp.equals(Constants.CUSTOMER)){
                    gotoForCustomer();
                }
                break;
        }
    }
}
