package com.teckdk.cuttiin.Main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class SelectBookDateActivity extends BaseActivity {

    ImageView imvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_book_date);

        loadLayout();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
        }
    }
}
