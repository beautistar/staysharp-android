package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.CustomView.CustomInfoWindow;
import com.teckdk.cuttiin.CustomView.CustomMapView;
import com.teckdk.cuttiin.Main.BarberShopActivity;
import com.teckdk.cuttiin.Main.BookActivity;
import com.teckdk.cuttiin.Main.MainCustomActivity;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class BarberShopFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnInfoWindowClickListener {

    Context context;

    CustomMapView mapView;
    ImageView imvMyLocation, imvSearch;
    RelativeLayout rytBarbershop;
    CustomInfoWindow customInfoWindow;
    LinearLayout lytBarbershop;
    View viewAlpha;

    GoogleMap googleMap;
    LatLng position = null;
    Marker marker;

    public BarberShopFragment(Context _context) {
        this.context = _context;
        position = new LatLng(Commons.lat, Commons.lng);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_barber_shop, container, false);

        mapView = (CustomMapView)fragment.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.onResume();

        imvMyLocation = (ImageView)fragment.findViewById(R.id.imvMyLocation);
        imvMyLocation.setOnClickListener(this);

        imvSearch =  (ImageView)fragment.findViewById(R.id.imvSearch);
        imvSearch.setOnClickListener(this);

        rytBarbershop = (RelativeLayout)fragment.findViewById(R.id.rytBarbershop);

        viewAlpha = (View)fragment.findViewById(R.id.viewAlpha);
        viewAlpha.setOnClickListener(this);

        lytBarbershop = (LinearLayout)fragment.findViewById(R.id.lytBarbershop);
        lytBarbershop.setOnClickListener(this);

        return fragment;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        googleMap.setMyLocationEnabled(false);
        googleMap.setOnInfoWindowClickListener(this);
        marker = googleMap.addMarker(new MarkerOptions().position(position));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(latLng));
            }
        });
        customInfoWindow = new CustomInfoWindow(context);
        mMap.setInfoWindowAdapter(customInfoWindow);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        googleMap.clear();
        marker = googleMap.addMarker(new MarkerOptions().position(latLng));
        marker.showInfoWindow();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        rytBarbershop.setVisibility(View.VISIBLE);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvMyLocation:
                break;
            case R.id.imvSearch:
                ((MainCustomActivity)context).showSearchFragment();
                break;
            case R.id.viewAlpha:
                rytBarbershop.setVisibility(View.GONE);
                break;
            case R.id.lytBarbershop:
                context.startActivity(new Intent(context, BarberShopActivity.class));
                break;
        }
    }
}
