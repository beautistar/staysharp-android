package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class WorkHoursModel implements Serializable {
    String openingTimeHour = "", openingTimeMinute = "", openingTimeSecond = "", closingTimeHour = "",  closingTimeMinute = ""
            , closingTimeSecond = "", minutesBeforeClosing = "", dateCreated = "", timezone = "", calendar = "", local = "";

    public WorkHoursModel() {}

    public WorkHoursModel(String openingTimeHour, String openingTimeMinute, String openingTimeSecond, String closingTimeHour, String closingTimeMinute, String closingTimeSecond, String minutesBeforeClosing, String dateCreated, String timezone, String calendar, String local) {
        this.openingTimeHour = openingTimeHour;
        this.openingTimeMinute = openingTimeMinute;
        this.openingTimeSecond = openingTimeSecond;
        this.closingTimeHour = closingTimeHour;
        this.closingTimeMinute = closingTimeMinute;
        this.closingTimeSecond = closingTimeSecond;
        this.minutesBeforeClosing = minutesBeforeClosing;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getOpeningTimeHour() {
        return openingTimeHour;
    }

    public void setOpeningTimeHour(String openingTimeHour) {
        this.openingTimeHour = openingTimeHour;
    }

    public String getOpeningTimeMinute() {
        return openingTimeMinute;
    }

    public void setOpeningTimeMinute(String openingTimeMinute) {
        this.openingTimeMinute = openingTimeMinute;
    }

    public String getOpeningTimeSecond() {
        return openingTimeSecond;
    }

    public void setOpeningTimeSecond(String openingTimeSecond) {
        this.openingTimeSecond = openingTimeSecond;
    }

    public String getClosingTimeHour() {
        return closingTimeHour;
    }

    public void setClosingTimeHour(String closingTimeHour) {
        this.closingTimeHour = closingTimeHour;
    }

    public String getClosingTimeMinute() {
        return closingTimeMinute;
    }

    public void setClosingTimeMinute(String closingTimeMinute) {
        this.closingTimeMinute = closingTimeMinute;
    }

    public String getClosingTimeSecond() {
        return closingTimeSecond;
    }

    public void setClosingTimeSecond(String closingTimeSecond) {
        this.closingTimeSecond = closingTimeSecond;
    }

    public String getMinutesBeforeClosing() {
        return minutesBeforeClosing;
    }

    public void setMinutesBeforeClosing(String minutesBeforeClosing) {
        this.minutesBeforeClosing = minutesBeforeClosing;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
