package com.teckdk.cuttiin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fuzzproductions.ratingbar.RatingBar;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.R;

import java.util.ArrayList;

/**
 * Created by STS on 5/27/2018.
 */

public class BarbersAdapter extends BaseAdapter {

    Context context;

    ArrayList<UserModel> allData =  new ArrayList<>();

    public BarbersAdapter(Context _context) {
        this.context = _context;
    }

    public void clearAll(){
        allData.clear();
        notifyDataSetChanged();
    }

    public void add(UserModel data){
        allData.add(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public UserModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){
            holder = new CustomHolder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_barber, parent, false);

            holder.setId(convertView);
            holder.setValue(allData.get(position));

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    class  CustomHolder{

        RoundedImageView imvPhoto;
        TextView txvUserName, txvDate;
        RatingBar rtbRating;

        private void setId(View view){
            imvPhoto = (RoundedImageView)view.findViewById(R.id.imvPhoto);
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            txvDate = (TextView)view.findViewById(R.id.txvDate);
            rtbRating = (RatingBar)view.findViewById(R.id.rtbRating);
        }

        private void setValue(UserModel mUserModel){
            if (!mUserModel.getProfileImageUrl().equals(Constants.NIL) || mUserModel.getProfileImageUrl().length() > 0) {
                Picasso.get().load(mUserModel.getProfileImageUrl()).into(imvPhoto);
            }

            txvUserName.setText(mUserModel.getFirstName() + " " + mUserModel.getLastName());
            txvDate.setText(mUserModel.getDateAccountCreated());
            rtbRating.setRating(Float.parseFloat(mUserModel.getRating()));
        }
    }
}
