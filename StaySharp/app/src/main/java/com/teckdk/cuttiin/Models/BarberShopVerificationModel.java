package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class BarberShopVerificationModel implements Serializable {

    String barberShopID = "", isBarberShopVerified = "", dateCreated = "", timezone = "", calendar= "", local = "";

    public BarberShopVerificationModel(){}

    public BarberShopVerificationModel(String barberShopID, String isBarberShopVerified, String dateCreated, String timezone, String calendar, String local) {
        this.barberShopID = barberShopID;
        this.isBarberShopVerified = isBarberShopVerified;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getBarberShopID() {
        return barberShopID;
    }

    public void setBarberShopID(String barberShopID) {
        this.barberShopID = barberShopID;
    }

    public String getIsBarberShopVerified() {
        return isBarberShopVerified;
    }

    public void setIsBarberShopVerified(String isBarberShopVerified) {
        this.isBarberShopVerified = isBarberShopVerified;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}

