package com.teckdk.cuttiin.Main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class UploadPictureActivity extends BaseActivity {

    TextView txvClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_picture);

        loadLayout();
    }

    private void loadLayout() {
        txvClose = (TextView)findViewById(R.id.txvClose);
        txvClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txvClose:
                finish();
                break;
        }
    }
}
