package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class UserModel implements Serializable {
    String firstName = "", lastName = "", email = "", mobileNumber = "", uniqueID = "", role = ""
            , rating = "", ratingValue = "", currencyCode = "", profileImageName = "", profileImageUrl = ""
            , barberShopID = "", companyName = "", companyLogoImageName = "", companyLogoImageUrl = ""
            , companyCoverPhotoUrl = "", companyCoverPhotoName = "", cvrNumber = "", accountNumerRegNr = ""
            , accountNumerKontoNr = "", companyFormattedAddress = "", companyAddressLongitude = ""
            , companyAddressLatitude = "", dateAccountCreated = "", timezone = "", calendar = "", local = ""
            , cardID = "", cardDateCreated = "", cardDateCalendar = "", cardDateTimeZone = "", cardDateLocale = ""
            , cardHasBeenDeleted = "", authentication = "", isDeleted = "", emailSent = "";

    public UserModel() { }

    public UserModel(String firstName, String lastName, String email, String mobileNumber, String uniqueID, String role, String rating, String ratingValue, String currencyCode, String profileImageName, String profileImageUrl, String barberShopID, String companyName, String companyLogoImageName, String companyLogoImageUrl, String companyCoverPhotoUrl, String companyCoverPhotoName, String cvrNumber, String accountNumerRegNr, String accountNumerKontoNr, String companyFormattedAddress, String companyAddressLongitude, String companyAddressLatitude, String dateAccountCreated, String timezone, String calendar, String local, String cardID, String cardDateCreated, String cardDateCalendar, String cardDateTimeZone, String cardDateLocale, String cardHasBeenDeleted, String authentication, String isDeleted, String emailSent) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.uniqueID = uniqueID;
        this.role = role;
        this.rating = rating;
        this.ratingValue = ratingValue;
        this.currencyCode = currencyCode;
        this.profileImageName = profileImageName;
        this.profileImageUrl = profileImageUrl;
        this.barberShopID = barberShopID;
        this.companyName = companyName;
        this.companyLogoImageName = companyLogoImageName;
        this.companyLogoImageUrl = companyLogoImageUrl;
        this.companyCoverPhotoUrl = companyCoverPhotoUrl;
        this.companyCoverPhotoName = companyCoverPhotoName;
        this.cvrNumber = cvrNumber;
        this.accountNumerRegNr = accountNumerRegNr;
        this.accountNumerKontoNr = accountNumerKontoNr;
        this.companyFormattedAddress = companyFormattedAddress;
        this.companyAddressLongitude = companyAddressLongitude;
        this.companyAddressLatitude = companyAddressLatitude;
        this.dateAccountCreated = dateAccountCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
        this.cardID = cardID;
        this.cardDateCreated = cardDateCreated;
        this.cardDateCalendar = cardDateCalendar;
        this.cardDateTimeZone = cardDateTimeZone;
        this.cardDateLocale = cardDateLocale;
        this.cardHasBeenDeleted = cardHasBeenDeleted;
        this.authentication = authentication;
        this.isDeleted = isDeleted;
        this.emailSent = emailSent;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(String ratingValue) {
        this.ratingValue = ratingValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getProfileImageName() {
        return profileImageName;
    }

    public void setProfileImageName(String profileImageName) {
        this.profileImageName = profileImageName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getBarberShopID() {
        return barberShopID;
    }

    public void setBarberShopID(String barberShopID) {
        this.barberShopID = barberShopID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogoImageName() {
        return companyLogoImageName;
    }

    public void setCompanyLogoImageName(String companyLogoImageName) {
        this.companyLogoImageName = companyLogoImageName;
    }

    public String getCompanyLogoImageUrl() {
        return companyLogoImageUrl;
    }

    public void setCompanyLogoImageUrl(String companyLogoImageUrl) {
        this.companyLogoImageUrl = companyLogoImageUrl;
    }

    public String getCompanyCoverPhotoUrl() {
        return companyCoverPhotoUrl;
    }

    public void setCompanyCoverPhotoUrl(String companyCoverPhotoUrl) {
        this.companyCoverPhotoUrl = companyCoverPhotoUrl;
    }

    public String getCompanyCoverPhotoName() {
        return companyCoverPhotoName;
    }

    public void setCompanyCoverPhotoName(String companyCoverPhotoName) {
        this.companyCoverPhotoName = companyCoverPhotoName;
    }

    public String getCvrNumber() {
        return cvrNumber;
    }

    public void setCvrNumber(String cvrNumber) {
        this.cvrNumber = cvrNumber;
    }

    public String getAccountNumerRegNr() {
        return accountNumerRegNr;
    }

    public void setAccountNumerRegNr(String accountNumerRegNr) {
        this.accountNumerRegNr = accountNumerRegNr;
    }

    public String getAccountNumerKontoNr() {
        return accountNumerKontoNr;
    }

    public void setAccountNumerKontoNr(String accountNumerKontoNr) {
        this.accountNumerKontoNr = accountNumerKontoNr;
    }

    public String getCompanyFormattedAddress() {
        return companyFormattedAddress;
    }

    public void setCompanyFormattedAddress(String companyFormattedAddress) {
        this.companyFormattedAddress = companyFormattedAddress;
    }

    public String getCompanyAddressLongitude() {
        return companyAddressLongitude;
    }

    public void setCompanyAddressLongitude(String companyAddressLongitude) {
        this.companyAddressLongitude = companyAddressLongitude;
    }

    public String getCompanyAddressLatitude() {
        return companyAddressLatitude;
    }

    public void setCompanyAddressLatitude(String companyAddressLatitude) {
        this.companyAddressLatitude = companyAddressLatitude;
    }

    public String getDateAccountCreated() {
        return dateAccountCreated;
    }

    public void setDateAccountCreated(String dateAccountCreated) {
        this.dateAccountCreated = dateAccountCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getCardDateCreated() {
        return cardDateCreated;
    }

    public void setCardDateCreated(String cardDateCreated) {
        this.cardDateCreated = cardDateCreated;
    }

    public String getCardDateCalendar() {
        return cardDateCalendar;
    }

    public void setCardDateCalendar(String cardDateCalendar) {
        this.cardDateCalendar = cardDateCalendar;
    }

    public String getCardDateTimeZone() {
        return cardDateTimeZone;
    }

    public void setCardDateTimeZone(String cardDateTimeZone) {
        this.cardDateTimeZone = cardDateTimeZone;
    }

    public String getCardDateLocale() {
        return cardDateLocale;
    }

    public void setCardDateLocale(String cardDateLocale) {
        this.cardDateLocale = cardDateLocale;
    }

    public String getCardHasBeenDeleted() {
        return cardHasBeenDeleted;
    }

    public void setCardHasBeenDeleted(String cardHasBeenDeleted) {
        this.cardHasBeenDeleted = cardHasBeenDeleted;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(String emailSent) {
        this.emailSent = emailSent;
    }
}
