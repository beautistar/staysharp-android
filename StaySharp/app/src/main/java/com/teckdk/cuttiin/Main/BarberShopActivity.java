package com.teckdk.cuttiin.Main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.teckdk.cuttiin.Adapter.BarbersListAdapter;
import com.teckdk.cuttiin.Adapter.ServiceViewPagerAdapter;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class BarberShopActivity extends BaseActivity {

    BarbersListAdapter mBarbersListAdapter;
    ServiceViewPagerAdapter mServiceViewPagerAdapter;

    TextView txvBarbers, txvServices;
    ListView lstBarbers;
    LinearLayout lytServices;
    TabLayout tabServices;
    ViewPager vpServices;
    ImageView imvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barber_shop);

        loadLayout();
    }

    private void loadLayout() {
        imvBack =  (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        txvBarbers =  (TextView)findViewById(R.id.txvBarbers);
        txvBarbers.setOnClickListener(this);

        txvServices = (TextView)findViewById(R.id.txvServices);
        txvServices.setOnClickListener(this);

        mBarbersListAdapter = new BarbersListAdapter(this);
        lstBarbers = (ListView)findViewById(R.id.lstBarbers);
        lstBarbers.setAdapter(mBarbersListAdapter);
        lstBarbers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(BarberShopActivity.this, SelectBarberActivity.class));
            }
        });

        lytServices =  (LinearLayout)findViewById(R.id.lytServices);
        lytServices.setVisibility(View.GONE);

        mServiceViewPagerAdapter =  new ServiceViewPagerAdapter(this, getSupportFragmentManager());
        vpServices =  (ViewPager)findViewById(R.id.vpServices);
        vpServices.setAdapter(mServiceViewPagerAdapter);

        tabServices =  (TabLayout)findViewById(R.id.tabServices);
        tabServices.setupWithViewPager(vpServices);
        tabServices.setTabsFromPagerAdapter(mServiceViewPagerAdapter);
    }

    private void showBarbersList() {
        lstBarbers.setVisibility(View.VISIBLE);
        lytServices.setVisibility(View.GONE);

        txvBarbers.setTextColor(getResources().getColor(R.color.mainColor));
        txvBarbers.setBackgroundResource(R.drawable.bg_left_cornor_round);

        txvServices.setTextColor(getResources().getColor(R.color.white));
        txvServices.setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    private void showServicesList() {
        lstBarbers.setVisibility(View.GONE);
        lytServices.setVisibility(View.VISIBLE);

        txvBarbers.setTextColor(getResources().getColor(R.color.white));
        txvBarbers.setBackgroundColor(getResources().getColor(R.color.transparent));

        txvServices.setTextColor(getResources().getColor(R.color.mainColor));
        txvServices.setBackgroundResource(R.drawable.bg_right_cornor_round);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txvBarbers:
                showBarbersList();
                break;
            case R.id.txvServices:
                showServicesList();
                break;
            case R.id.imvBack:
                finish();
                break;
        }
    }
}
