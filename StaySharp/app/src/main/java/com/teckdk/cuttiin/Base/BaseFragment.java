package com.teckdk.cuttiin.Base;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;

/**
 * Created by HGS on 12/11/2015.
 */

public abstract class BaseFragment extends Fragment implements View.OnClickListener{

    public void showToast(String toast_string){
        Toast.makeText(getActivity(), toast_string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

    }
}
