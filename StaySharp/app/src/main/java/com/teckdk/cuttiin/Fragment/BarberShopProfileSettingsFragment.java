package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Main.BarberShopBarbersAndServicesActivity;
import com.teckdk.cuttiin.Main.BookingHistoryBarberShopActivity;
import com.teckdk.cuttiin.Main.LoginActivity;
import com.teckdk.cuttiin.Main.MainBarberActivity;
import com.teckdk.cuttiin.Main.OpeningHoursActivity;
import com.teckdk.cuttiin.Main.UpcomingBarberShopBookingActivity;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class BarberShopProfileSettingsFragment extends BaseFragment {

    Context context;

    LinearLayout lytBack, lytOpeningHours, lytBarberAndService, lytLogout;
    LinearLayout lytHistory, lytUpcoming, lytChangePassword;

    public BarberShopProfileSettingsFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_barber_shop_profile_settings, container, false);

        lytBack = (LinearLayout)fragment.findViewById(R.id.lytBack);
        lytBack.setOnClickListener(this);

        lytOpeningHours = (LinearLayout)fragment.findViewById(R.id.lytOpeningHours);
        lytOpeningHours.setOnClickListener(this);

        lytBarberAndService = (LinearLayout)fragment.findViewById(R.id.lytBarberAndService);
        lytBarberAndService.setOnClickListener(this);

        lytLogout = (LinearLayout)fragment.findViewById(R.id.lytLogout);
        lytLogout.setOnClickListener(this);

        lytHistory = (LinearLayout)fragment.findViewById(R.id.lytHistory);
        lytHistory.setOnClickListener(this);

        lytUpcoming = (LinearLayout)fragment.findViewById(R.id.lytUpcoming);
        lytUpcoming.setOnClickListener(this);

        lytChangePassword = (LinearLayout)fragment.findViewById(R.id.lytChangePassword);
        lytChangePassword.setOnClickListener(this);

        return fragment;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytBack:
                ((MainBarberActivity)context).showBarberProfileFragment();
                break;
            case R.id.lytOpeningHours:
                ((MainBarberActivity)context).startActivity(new Intent(
                        context, OpeningHoursActivity.class
                ));
                break;
            case R.id.lytBarberAndService:
                ((MainBarberActivity)context).startActivity(new Intent(
                        context, BarberShopBarbersAndServicesActivity.class
                ));
                break;
            case R.id.lytLogout:
                Preference.getInstance().put(context, PrefConst.PREFKEY_USEREMAIL, "");
                Preference.getInstance().put(context, PrefConst.PREFKEY_USERPWD, "");
                Commons.password = "";
                context.startActivity(new Intent((Activity) context, LoginActivity.class)); ((Activity) context).finish();
                FirebaseAuth.getInstance().signOut();
                break;
            case R.id.lytHistory:
                context.startActivity(new Intent(context, BookingHistoryBarberShopActivity.class));
                break;
            case R.id.lytUpcoming:
                context.startActivity(new Intent(context, UpcomingBarberShopBookingActivity.class));
                break;
            case R.id.lytChangePassword:
                ((MainBarberActivity)context).showChangePasswordForBarberShopFragment();
                break;
        }
    }
}
