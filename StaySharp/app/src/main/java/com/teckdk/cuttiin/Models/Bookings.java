package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class Bookings implements Serializable {
    String bookingID = "", customerID = "", bookedBarberID = "", bookedServiceID = "", bookedBarberShopID = ""
            , bookedServiceQuantity = "", bookingStartTime = "", bookingEndTime = "", confirmedTotalPrice = ""
            , totalPriceWithCharges = "", confirmedTotalTime = "", bookingDescriptionImageName = "", bookingDescriptionImageUrl = ""
            , bookingDescriptionText = "", paymentID = "", isCompleted = "", severDidNotMakeBooking = "", dateCreated = ""
            , timezone = "", calendar = "", local = "", bookingCancel = "", cancelDate = "", cancelTimeZone = "", cancelCalendar = ""
            , cancelLocal = "", isRemiderSet = "", isRated = "", actualEarning = "";

    public Bookings() { }

    public Bookings(String bookingID, String customerID, String bookedBarberID, String bookedServiceID, String bookedBarberShopID, String bookedServiceQuantity, String bookingStartTime, String bookingEndTime, String confirmedTotalPrice, String totalPriceWithCharges, String confirmedTotalTime, String bookingDescriptionImageName, String bookingDescriptionImageUrl, String bookingDescriptionText, String paymentID, String isCompleted, String severDidNotMakeBooking, String dateCreated, String timezone, String calendar, String local, String bookingCancel, String cancelDate, String cancelTimeZone, String cancelCalendar, String cancelLocal, String isRemiderSet, String isRated, String actualEarning) {
        this.bookingID = bookingID;
        this.customerID = customerID;
        this.bookedBarberID = bookedBarberID;
        this.bookedServiceID = bookedServiceID;
        this.bookedBarberShopID = bookedBarberShopID;
        this.bookedServiceQuantity = bookedServiceQuantity;
        this.bookingStartTime = bookingStartTime;
        this.bookingEndTime = bookingEndTime;
        this.confirmedTotalPrice = confirmedTotalPrice;
        this.totalPriceWithCharges = totalPriceWithCharges;
        this.confirmedTotalTime = confirmedTotalTime;
        this.bookingDescriptionImageName = bookingDescriptionImageName;
        this.bookingDescriptionImageUrl = bookingDescriptionImageUrl;
        this.bookingDescriptionText = bookingDescriptionText;
        this.paymentID = paymentID;
        this.isCompleted = isCompleted;
        this.severDidNotMakeBooking = severDidNotMakeBooking;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
        this.bookingCancel = bookingCancel;
        this.cancelDate = cancelDate;
        this.cancelTimeZone = cancelTimeZone;
        this.cancelCalendar = cancelCalendar;
        this.cancelLocal = cancelLocal;
        this.isRemiderSet = isRemiderSet;
        this.isRated = isRated;
        this.actualEarning = actualEarning;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getBookedBarberID() {
        return bookedBarberID;
    }

    public void setBookedBarberID(String bookedBarberID) {
        this.bookedBarberID = bookedBarberID;
    }

    public String getBookedServiceID() {
        return bookedServiceID;
    }

    public void setBookedServiceID(String bookedServiceID) {
        this.bookedServiceID = bookedServiceID;
    }

    public String getBookedBarberShopID() {
        return bookedBarberShopID;
    }

    public void setBookedBarberShopID(String bookedBarberShopID) {
        this.bookedBarberShopID = bookedBarberShopID;
    }

    public String getBookedServiceQuantity() {
        return bookedServiceQuantity;
    }

    public void setBookedServiceQuantity(String bookedServiceQuantity) {
        this.bookedServiceQuantity = bookedServiceQuantity;
    }

    public String getBookingStartTime() {
        return bookingStartTime;
    }

    public void setBookingStartTime(String bookingStartTime) {
        this.bookingStartTime = bookingStartTime;
    }

    public String getBookingEndTime() {
        return bookingEndTime;
    }

    public void setBookingEndTime(String bookingEndTime) {
        this.bookingEndTime = bookingEndTime;
    }

    public String getConfirmedTotalPrice() {
        return confirmedTotalPrice;
    }

    public void setConfirmedTotalPrice(String confirmedTotalPrice) {
        this.confirmedTotalPrice = confirmedTotalPrice;
    }

    public String getTotalPriceWithCharges() {
        return totalPriceWithCharges;
    }

    public void setTotalPriceWithCharges(String totalPriceWithCharges) {
        this.totalPriceWithCharges = totalPriceWithCharges;
    }

    public String getConfirmedTotalTime() {
        return confirmedTotalTime;
    }

    public void setConfirmedTotalTime(String confirmedTotalTime) {
        this.confirmedTotalTime = confirmedTotalTime;
    }

    public String getBookingDescriptionImageName() {
        return bookingDescriptionImageName;
    }

    public void setBookingDescriptionImageName(String bookingDescriptionImageName) {
        this.bookingDescriptionImageName = bookingDescriptionImageName;
    }

    public String getBookingDescriptionImageUrl() {
        return bookingDescriptionImageUrl;
    }

    public void setBookingDescriptionImageUrl(String bookingDescriptionImageUrl) {
        this.bookingDescriptionImageUrl = bookingDescriptionImageUrl;
    }

    public String getBookingDescriptionText() {
        return bookingDescriptionText;
    }

    public void setBookingDescriptionText(String bookingDescriptionText) {
        this.bookingDescriptionText = bookingDescriptionText;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public String getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(String isCompleted) {
        this.isCompleted = isCompleted;
    }

    public String getSeverDidNotMakeBooking() {
        return severDidNotMakeBooking;
    }

    public void setSeverDidNotMakeBooking(String severDidNotMakeBooking) {
        this.severDidNotMakeBooking = severDidNotMakeBooking;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getBookingCancel() {
        return bookingCancel;
    }

    public void setBookingCancel(String bookingCancel) {
        this.bookingCancel = bookingCancel;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelTimeZone() {
        return cancelTimeZone;
    }

    public void setCancelTimeZone(String cancelTimeZone) {
        this.cancelTimeZone = cancelTimeZone;
    }

    public String getCancelCalendar() {
        return cancelCalendar;
    }

    public void setCancelCalendar(String cancelCalendar) {
        this.cancelCalendar = cancelCalendar;
    }

    public String getCancelLocal() {
        return cancelLocal;
    }

    public void setCancelLocal(String cancelLocal) {
        this.cancelLocal = cancelLocal;
    }

    public String getIsRemiderSet() {
        return isRemiderSet;
    }

    public void setIsRemiderSet(String isRemiderSet) {
        this.isRemiderSet = isRemiderSet;
    }

    public String getIsRated() {
        return isRated;
    }

    public void setIsRated(String isRated) {
        this.isRated = isRated;
    }

    public String getActualEarning() {
        return actualEarning;
    }

    public void setActualEarning(String actualEarning) {
        this.actualEarning = actualEarning;
    }
}
