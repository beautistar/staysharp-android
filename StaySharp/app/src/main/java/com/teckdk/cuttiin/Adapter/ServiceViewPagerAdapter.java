package com.teckdk.cuttiin.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.teckdk.cuttiin.Fragment.GentsFragment;
import com.teckdk.cuttiin.Fragment.KidsFragment;
import com.teckdk.cuttiin.Fragment.LadiesFragment;

/**
 * Created by kundan on 10/16/2015.
 */
public class ServiceViewPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;

    public ServiceViewPagerAdapter(Context _context, FragmentManager fm ) {

        super(fm);
        this.context = _context;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment frag=null;
        switch (position){

            case 0:
                frag=new LadiesFragment(context);
                break;

            case 1:
                frag = new GentsFragment(context);
                break;

            case 2:
                frag=new KidsFragment(context);
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:
                title="LADIES(0)";
                break;
            case 1:
                title="GENTS(5)";
                break;
            case 2:
                title="KIDS(1)";
                break;
        }

        return title;
    }

}
