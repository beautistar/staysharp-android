package com.teckdk.cuttiin.Main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.WorkHoursModel;
import com.teckdk.cuttiin.R;

public class OpeningHoursActivity extends BaseActivity {

    Button btnMon, btnTue, btnWed, btnThu, btnFri, btnSat, btnSun, btnNext;
    TextView txvTitle;
    ImageView imvBack;

    FirebaseUser currentUser;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening_hours);

        loadLayout();

        init();
    }

    private void init() {
        mAuth =  FirebaseAuth.getInstance();
        currentUser  = mAuth.getCurrentUser();
    }

    private void loadLayout() {
        btnMon = (Button)findViewById(R.id.btnMon);
        btnMon.setOnClickListener(this);

        btnTue = (Button)findViewById(R.id.btnTue);
        btnTue.setOnClickListener(this);

        btnWed = (Button)findViewById(R.id.btnWed);
        btnWed.setOnClickListener(this);

        btnThu = (Button)findViewById(R.id.btnThu);
        btnThu.setOnClickListener(this);

        btnFri = (Button)findViewById(R.id.btnFri);
        btnFri.setOnClickListener(this);

        btnSat = (Button)findViewById(R.id.btnSat);
        btnSat.setOnClickListener(this);

        btnSun = (Button)findViewById(R.id.btnSun);
        btnSun.setOnClickListener(this);

        btnNext = (Button)findViewById(R.id.btnNext); btnNext.setClickable(false);
        btnNext.setOnClickListener(this);

        txvTitle = (TextView)findViewById(R.id.txvTitle);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);
    }

    private void showSelectHourActivity(String selectedDay){
        Intent intent = new Intent(this, DayTimeChoiceActivity.class);
        intent.putExtra(Constants.SELECTED_DAY, selectedDay);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Commons.loggedIn){
            btnNext.setVisibility(View.GONE);
            imvBack.setVisibility(View.VISIBLE);
            txvTitle.setText("Opening and Closing Times");
        }else {
            imvBack.setVisibility(View.GONE);
            txvTitle.setText("Registration: Step 2 of 5");
            btnNext.setVisibility(View.VISIBLE);
        }

        Commons.DATABASE_WORK_HOURS_REF.child(currentUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot dsp : dataSnapshot.getChildren()){
                            setDayValues(dsp.getKey(), dsp.getValue(WorkHoursModel.class));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void setDayValues(String key, WorkHoursModel mWorkHoursModel) {
        String openAndCloseTimes = "OPEN: " + mWorkHoursModel.getOpeningTimeHour() + ":" + mWorkHoursModel.getOpeningTimeMinute()
                + " - CLOSE: " + mWorkHoursModel.getClosingTimeHour() + ":" + mWorkHoursModel.getClosingTimeMinute();
        switch (key){
            case Constants.MON:
                btnMon.setText("MON, " + openAndCloseTimes);
                break;
            case Constants.TUE:
                btnTue.setText("TUE, " + openAndCloseTimes);
                break;
            case Constants.WED:
                btnWed.setText("WED, " + openAndCloseTimes);
                break;
            case Constants.THU:
                btnThu.setText("THU, " + openAndCloseTimes);
                break;
            case Constants.FRI:
                btnFri.setText("FRI, " + openAndCloseTimes);
                break;
            case Constants.SAT:
                btnSat.setText("SAT, " + openAndCloseTimes);
                break;
            case Constants.SUN:
                btnSun.setText("SUN, " + openAndCloseTimes);
                break;
        }

        btnNext.setText("Done");
        btnNext.setClickable(true);
        btnNext.setAlpha(1.0f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnMon:
                showSelectHourActivity(Constants.MON);
                break;
            case R.id.btnTue:
                showSelectHourActivity(Constants.TUE);
                break;
            case R.id.btnWed:
                showSelectHourActivity(Constants.WED);
                break;
            case R.id.btnThu:
                showSelectHourActivity(Constants.TUE);
                break;
            case R.id.btnFri:
                showSelectHourActivity(Constants.FRI);
                break;
            case R.id.btnSat:
                showSelectHourActivity(Constants.SAT);
                break;
            case R.id.btnSun:
                showSelectHourActivity(Constants.SUN);
                break;
            case R.id.btnNext:
                startActivity(new Intent(this, AddServiceActivity.class)); finish();
                break;
            case R.id.imvBack:
                finish();
                break;
        }
    }
}
