package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class BarberShopModel implements Serializable {
    String uniqueID = "", companyName = "",  email = "", role = "", companyLogoImageUrl = ""
            , mobileNumber = "", rating = "", companyLogoImageName = "", companyCoverPhotoName = ""
            , companyCoverPhotoUrl = "", accountNumerRegNr = "", accountNumerKontoNr = "", companyFormattedAddress = ""
            , companyAddressLongitude = "", companyAddressLatitude = "", dateAccountCreated = "", timezone = ""
            , calendar = "",  local = "", currencyCode = "";

    public BarberShopModel(){};

    public BarberShopModel(String uniqueID, String companyName, String email, String role, String companyLogoImageUrl, String mobileNumber, String rating, String companyLogoImageName, String companyCoverPhotoName, String companyCoverPhotoUrl, String accountNumerRegNr, String accountNumerKontoNr, String companyFormattedAddress, String companyAddressLongitude, String companyAddressLatitude, String dateAccountCreated, String timezone, String calendar, String local, String currencyCode) {
        this.uniqueID = uniqueID;
        this.companyName = companyName;
        this.email = email;
        this.role = role;
        this.companyLogoImageUrl = companyLogoImageUrl;
        this.mobileNumber = mobileNumber;
        this.rating = rating;
        this.companyLogoImageName = companyLogoImageName;
        this.companyCoverPhotoName = companyCoverPhotoName;
        this.companyCoverPhotoUrl = companyCoverPhotoUrl;
        this.accountNumerRegNr = accountNumerRegNr;
        this.accountNumerKontoNr = accountNumerKontoNr;
        this.companyFormattedAddress = companyFormattedAddress;
        this.companyAddressLongitude = companyAddressLongitude;
        this.companyAddressLatitude = companyAddressLatitude;
        this.dateAccountCreated = dateAccountCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
        this.currencyCode = currencyCode;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompanyLogoImageUrl() {
        return companyLogoImageUrl;
    }

    public void setCompanyLogoImageUrl(String companyLogoImageUrl) {
        this.companyLogoImageUrl = companyLogoImageUrl;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCompanyLogoImageName() {
        return companyLogoImageName;
    }

    public void setCompanyLogoImageName(String companyLogoImageName) {
        this.companyLogoImageName = companyLogoImageName;
    }

    public String getCompanyCoverPhotoName() {
        return companyCoverPhotoName;
    }

    public void setCompanyCoverPhotoName(String companyCoverPhotoName) {
        this.companyCoverPhotoName = companyCoverPhotoName;
    }

    public String getCompanyCoverPhotoUrl() {
        return companyCoverPhotoUrl;
    }

    public void setCompanyCoverPhotoUrl(String companyCoverPhotoUrl) {
        this.companyCoverPhotoUrl = companyCoverPhotoUrl;
    }

    public String getAccountNumerRegNr() {
        return accountNumerRegNr;
    }

    public void setAccountNumerRegNr(String accountNumerRegNr) {
        this.accountNumerRegNr = accountNumerRegNr;
    }

    public String getAccountNumerKontoNr() {
        return accountNumerKontoNr;
    }

    public void setAccountNumerKontoNr(String accountNumerKontoNr) {
        this.accountNumerKontoNr = accountNumerKontoNr;
    }

    public String getCompanyFormattedAddress() {
        return companyFormattedAddress;
    }

    public void setCompanyFormattedAddress(String companyFormattedAddress) {
        this.companyFormattedAddress = companyFormattedAddress;
    }

    public String getCompanyAddressLongitude() {
        return companyAddressLongitude;
    }

    public void setCompanyAddressLongitude(String companyAddressLongitude) {
        this.companyAddressLongitude = companyAddressLongitude;
    }

    public String getCompanyAddressLatitude() {
        return companyAddressLatitude;
    }

    public void setCompanyAddressLatitude(String companyAddressLatitude) {
        this.companyAddressLatitude = companyAddressLatitude;
    }

    public String getDateAccountCreated() {
        return dateAccountCreated;
    }

    public void setDateAccountCreated(String dateAccountCreated) {
        this.dateAccountCreated = dateAccountCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}

