package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class BarberCashPaymentModel implements Serializable {
    String barberShopID = "", isCashPaymentAllowed = "", dateCreated = "", timezone = "", calendar = "", local = "";

    public BarberCashPaymentModel(){}

    public BarberCashPaymentModel(String barberShopID, String isCashPaymentAllowed, String dateCreated, String timezone, String calendar, String local) {


        this.barberShopID = barberShopID;
        this.isCashPaymentAllowed = isCashPaymentAllowed;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getBarberShopID() {
        return barberShopID;
    }

    public void setBarberShopID(String barberShopID) {
        this.barberShopID = barberShopID;
    }

    public String getIsCashPaymentAllowed() {
        return isCashPaymentAllowed;
    }

    public void setIsCashPaymentAllowed(String isCashPaymentAllowed) {
        this.isCashPaymentAllowed = isCashPaymentAllowed;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
