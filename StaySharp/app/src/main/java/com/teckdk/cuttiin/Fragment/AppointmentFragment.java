package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class AppointmentFragment extends BaseFragment {

    Context context;

    public AppointmentFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_appointment, container, false);

        return fragment;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
