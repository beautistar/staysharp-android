package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Main.MainBarberActivity;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.BitmapUtils;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.io.File;
import java.io.InputStream;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class EditBarberShopProfileFragment extends BaseFragment {

    Context context;

    LinearLayout lytBack;
    RoundedImageView imvPhoto;
    Button btnSave;
    EditText edtBarberShopName, edtEmail, edtPhone;
    TextView txvAddress;

    FirebaseStorage storage;
    StorageReference photoRef;
    private DatabaseReference mDatabase;

    private Uri imageCaptureUri;
    String photoPath = "", companyLogoImageName = "", email = "", companyLogoImageUrl = "";
    String companyLatitude = "", companyLongitude = "", companyName = "", companyAddress = "", phoneNumber = "";

    public EditBarberShopProfileFragment(Context _context) {
        this.context = _context;

        storage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        companyLogoImageUrl =  Commons.mUserModel.getCompanyLogoImageUrl();
        companyName =  Commons.mUserModel.getCompanyName();
        companyAddress = Commons.mUserModel.getCompanyFormattedAddress();
        phoneNumber = Commons.mUserModel.getMobileNumber();
        email = Commons.mUserModel.getEmail();
        companyLogoImageName = Commons.mUserModel.getCompanyLogoImageName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_edit_barber_shop_profile, container, false);

        lytBack = (LinearLayout)fragment.findViewById(R.id.lytBack);
        lytBack.setOnClickListener(this);

        edtPhone = (EditText)fragment.findViewById(R.id.edtPhone);
        edtPhone.setText(Commons.mUserModel.getMobileNumber());
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                phoneNumber = edtPhone.getText().toString().trim();
                ((MainBarberActivity)context).changeButtonStyle_1(btnSave, true);
            }
        });

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);
        if (!Commons.mUserModel.getCompanyLogoImageUrl().equals(Constants.NIL) && Commons.mUserModel.getCompanyLogoImageUrl().length() >  0){
            Picasso.get().load(Commons.mUserModel.getCompanyLogoImageUrl()).into(imvPhoto);
        }

        btnSave = (Button)fragment.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        edtBarberShopName = (EditText)fragment.findViewById(R.id.edtBarberShopName);
        edtBarberShopName.setText(Commons.mUserModel.getCompanyName());
        edtBarberShopName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                companyName =  edtBarberShopName.getText().toString().trim();
                ((MainBarberActivity)context).changeButtonStyle_1(btnSave, true);
            }
        });

        edtEmail = (EditText)fragment.findViewById(R.id.edtEmail);
        edtEmail.setText(Commons.mUserModel.getEmail());
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                ((MainBarberActivity)context).changeButtonStyle_1(btnSave, true);
            }
        });

        txvAddress =  (TextView)fragment.findViewById(R.id.txvAddress);
        txvAddress.setOnClickListener(this);
        txvAddress.setText(Commons.mUserModel.getCompanyFormattedAddress());

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void selectPicture() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();
                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {
                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(context);

                        InputStream in = context.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        companyLogoImageName = saveFile.getName();

                        ((MainBarberActivity)context).changeButtonStyle_1(btnSave, true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:
                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA: {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(context, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(context)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }

            case Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(context, data);
                    txvAddress.setText(place.getName());
                    companyAddress = txvAddress.getText().toString().trim();

                    ((MainBarberActivity)context).changeButtonStyle_1(btnSave, true);
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(context, data);
                } else if (resultCode == RESULT_CANCELED) {}
                break;
        }
    }

    private void getCompanyAddress() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
            startActivityForResult(intent, Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    private void processSaveProfile() {
        if (photoPath.length() > 0){
            showProgress();
            photoRef = storage.getReference().child(Constants.COMPANY_LOGO).child(Commons.mUserModel.getUniqueID() + ".jpeg");

            UploadTask uploadTask = photoRef.putFile(imageCaptureUri);
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    closeProgress();
                    if (!task.isSuccessful()) {
                        LogUtil.e(Constants.TAG + "Failed_00");
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return photoRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        companyLogoImageUrl = task.getResult().toString();
                        updateBarbershopProfile();
                    } else {
                        // Handle failures
                        showToast("Upload failed");
                        closeProgress();
                    }
                }
            });
        }else {
            updateBarbershopProfile();
        }
    }

    private void updateBarbershopProfile() {
        closeProgress();

        companyLatitude = String.valueOf(Commons.lat);
        companyLongitude = String.valueOf(Commons.lng);

        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("companyLogoImageUrl").setValue(companyLogoImageUrl);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("companyName").setValue(companyName);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("companyFormattedAddress").setValue(companyAddress);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("companyAddressLongitude").setValue(companyLongitude);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("companyAddressLatitude").setValue(companyLatitude);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("mobileNumber").setValue(phoneNumber);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("email").setValue(email);
        mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("companyLogoImageName").setValue(companyLogoImageName);

        Commons.mUserModel.setCompanyLogoImageUrl(companyLogoImageUrl);
        Commons.mUserModel.setCompanyName(companyName);
        Commons.mUserModel.setCompanyFormattedAddress(companyAddress);
        Commons.mUserModel.setCompanyAddressLongitude(companyLongitude);
        Commons.mUserModel.setCompanyAddressLatitude(companyLatitude);
        Commons.mUserModel.setMobileNumber(phoneNumber);
        Commons.mUserModel.setEmail(email);
        Commons.mUserModel.setCompanyLogoImageName(companyLogoImageName);

        ((MainBarberActivity)context).changeButtonStyle_1(btnSave, false);
    }

    private void showProgress(){
        ((MainBarberActivity)context).showProgress();
    }

    private void closeProgress(){
        ((MainBarberActivity)context).closeProgress();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytBack:
                ((MainBarberActivity)context).showBarberProfileFragment();
                break;
            case R.id.imvPhoto:
                selectPicture();
                break;
            case R.id.btnSave:
                processSaveProfile();
                break;
            case R.id.txvAddress:
                getCompanyAddress();
                break;
        }
    }
}
