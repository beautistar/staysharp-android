package com.teckdk.cuttiin.Main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;

public class LoginWithEmailActivity extends BaseActivity {

    ImageView imvBack;
    TextView txvShowHide, txvForgotPwd, txvError;
    EditText edtPassword, edtEmail;
    Button btnLogin;

    private FirebaseAuth mAuth;

    String email = "", password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_email);

        init();

        loadLayout();
    }

    private void init() {
        email = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USEREMAIL, "");
        password = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");
        Commons.password = password;

        mAuth = FirebaseAuth.getInstance();
    }

    private void loadLayout() {

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        btnLogin.setClickable(false);

        txvError = (TextView)findViewById(R.id.txvError);

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                changeButtonStyle(btnLogin, (email.length() > 0 && password.length() > 0));
            }
        });

        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                password = edtPassword.getText().toString().trim();
                changeButtonStyle(btnLogin, (email.length() > 0 && password.length() >  0));
            }
        });

        txvShowHide = (TextView)findViewById(R.id.txvShowHide);
        txvShowHide.setOnClickListener(this);

        txvForgotPwd = (TextView)findViewById(R.id.txvForgotPwd);
        txvForgotPwd.setOnClickListener(this);

        if (email.length() > 0 && password.length() > 0 ){
            edtEmail.setText(email);
            edtPassword.setText(password);
            loginProcess();
        }
    }

    private void loginProcess() {
        showProgress();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            LogUtil.e(Constants.TAG + "signInWithEmail:success");
                            getUserModel(mAuth.getCurrentUser().getUid());
                        } else {
                            closeProgress();
                            String errorCode = task.getException().getMessage();
                            txvError.setText(errorCode);
                        }
                    }
                });

    }

    private void getUserModel(final String uid) {
        Commons.DATABASE_USER_REF.child(uid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        closeProgress();

                        Commons.mUserModel =  dataSnapshot.getValue(UserModel.class);

                        Preference.getInstance().put(LoginWithEmailActivity.this, PrefConst.PREFKEY_USEREMAIL, email);
                        Preference.getInstance().put(LoginWithEmailActivity.this, PrefConst.PREFKEY_USERPWD, password);
                        Commons.password = password;

                        Commons.DATABASE_USER_REF.child(uid).removeEventListener(this);

                        if (Commons.mUserModel.getRole().equals(Constants.USER_CUSTOMER)){
                            Commons.loggedIn = true;
                            startActivity(new Intent(LoginWithEmailActivity.this, MainCustomActivity.class)); finish();
                        }else if (Commons.mUserModel.getRole().equals(Constants.USER_BARBER_SHOP) ||
                                Commons.mUserModel.getRole().equals(Constants.USER_BARBER_STAFF)){
                            Commons.loggedIn = true;
                            startActivity(new Intent(LoginWithEmailActivity.this, MainBarberActivity.class)); finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        closeProgress();
                        txvError.setText(databaseError.getMessage());
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                startActivity(new Intent(this, LoginActivity.class)); finish();
                break;
            case R.id.txvShowHide:
                txvShowHide.setSelected(!txvShowHide.isSelected());
                if (txvShowHide.isSelected()){
                    txvShowHide.setText("Hide");
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                }else {
                    txvShowHide.setText("Show");
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                break;
            case R.id.txvForgotPwd:
                startActivity(new Intent(this, SendEmailActivity.class));
                break;
            case R.id.btnLogin:
                loginProcess();
                break;
        }
    }
}
