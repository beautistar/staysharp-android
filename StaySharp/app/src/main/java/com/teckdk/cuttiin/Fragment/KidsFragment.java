package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teckdk.cuttiin.Adapter.ServiceListAdapter;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Main.BookActivity;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class KidsFragment extends BaseFragment {

    Context context;
    ServiceListAdapter mServiceListAdapter;

    ListView lstService;

    public KidsFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_kids, container, false);

        mServiceListAdapter =  new ServiceListAdapter(context);
        lstService =  (ListView)fragment.findViewById(R.id.lstService);
        lstService.setAdapter(mServiceListAdapter);
        lstService.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                context.startActivity(new Intent(context, BookActivity.class));
            }
        });

        return fragment;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
