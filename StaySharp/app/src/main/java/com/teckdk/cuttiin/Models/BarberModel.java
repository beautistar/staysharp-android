package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class BarberModel implements Serializable {
    String uniqueID = "", firstName = "", lastName = "", email = "", role = "", barberShopID = ""
            , profileImageUrl = "", mobileNumber = "", profileImageName = "", dateAccountCreated = ""
            , timezone = "", calendar = "", local = "", rating = "", isDeleted = "";

    public BarberModel() {}

    public BarberModel(String uniqueID, String firstName, String lastName, String email, String role, String barberShopID, String profileImageUrl, String mobileNumber, String profileImageName, String dateAccountCreated, String timezone, String calendar, String local, String rating, String isDeleted) {
        this.uniqueID = uniqueID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.barberShopID = barberShopID;
        this.profileImageUrl = profileImageUrl;
        this.mobileNumber = mobileNumber;
        this.profileImageName = profileImageName;
        this.dateAccountCreated = dateAccountCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
        this.rating = rating;
        this.isDeleted = isDeleted;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBarberShopID() {
        return barberShopID;
    }

    public void setBarberShopID(String barberShopID) {
        this.barberShopID = barberShopID;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getProfileImageName() {
        return profileImageName;
    }

    public void setProfileImageName(String profileImageName) {
        this.profileImageName = profileImageName;
    }

    public String getDateAccountCreated() {
        return dateAccountCreated;
    }

    public void setDateAccountCreated(String dateAccountCreated) {
        this.dateAccountCreated = dateAccountCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
}
