package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fuzzproductions.ratingbar.RatingBar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Main.MainBarberActivity;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class BarberProfileFragment extends BaseFragment {

    Context context;

    RoundedImageView imvPhoto;
    TextView txvUserName, txvAddress, txvRatings, txvUniqueId;
    RatingBar rtbRating;
    RelativeLayout rytProfile;
    LinearLayout lytSettings;

    public BarberProfileFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_barber_profeil, container, false);

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);

        txvUserName = (TextView)fragment.findViewById(R.id.txvUserName);

        txvAddress = (TextView)fragment.findViewById(R.id.txvAddress);
        txvAddress.setText(Commons.mUserModel.getCompanyFormattedAddress());

        txvRatings = (TextView)fragment.findViewById(R.id.txvRatings);
        txvRatings.setText(Commons.mUserModel.getRating() + " Reviews");

        txvUniqueId = (TextView)fragment.findViewById(R.id.txvUniqueId);
        txvUniqueId.setText("Unique ID: " + Commons.mUserModel.getUniqueID());

        rytProfile = (RelativeLayout)fragment.findViewById(R.id.rytProfile);
        rytProfile.setOnClickListener(this);

        lytSettings =  (LinearLayout)fragment.findViewById(R.id.lytSettings);
        lytSettings.setOnClickListener(this);

        rtbRating = (RatingBar)fragment.findViewById(R.id.rtbRating);

        switch (Commons.mUserModel.getRole()){
            case Constants.USER_BARBER_STAFF:
                if (!Commons.mUserModel.getProfileImageUrl().equals(Constants.NIL) && Commons.mUserModel.getProfileImageUrl().length() >  0){
                    Picasso.get().load(Commons.mUserModel.getProfileImageUrl()).into(imvPhoto);
                }
                txvUserName.setText(Commons.mUserModel.getFirstName() + " " + Commons.mUserModel.getLastName());
                getBarberStaffRatings();
                break;
            case Constants.USER_BARBER_SHOP:
                if (!Commons.mUserModel.getCompanyLogoImageUrl().equals(Constants.NIL) && Commons.mUserModel.getCompanyLogoImageUrl().length() >  0){
                    Picasso.get().load(Commons.mUserModel.getCompanyLogoImageUrl()).into(imvPhoto);
                }
                txvUserName.setText(Commons.mUserModel.getCompanyName());
                getBarberShopRatings();
                break;
        }

        return fragment;
    }

    private void getBarberShopRatings() {
        showProgress();
        Commons.RATING_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        txvRatings.setText(String.valueOf(dataSnapshot.getChildrenCount()) + " Reviews");
                        rtbRating.setRating(dataSnapshot.getChildrenCount());
                        closeProgress();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        closeProgress();
                    }
                });
    }

    private void getBarberStaffRatings() {
        Commons.RATING_REF.child(Commons.mUserModel.getUniqueID())
            .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
    }

    private void showProgress(){
        ((MainBarberActivity)context).showProgress();
    }
    private void closeProgress(){
        ((MainBarberActivity)context).closeProgress();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rytProfile:
                ((MainBarberActivity)context).showEditBarberProfileFragment();
                break;
            case R.id.lytSettings:
                ((MainBarberActivity)context).showBarberShopProfileSettingsFragment();
                break;

        }
    }
}
