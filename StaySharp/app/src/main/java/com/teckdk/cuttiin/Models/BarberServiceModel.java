package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class BarberServiceModel implements Serializable {

    String serviceID = "", dateCreated = "", timezone = "", calendar = "", local = "";

    public BarberServiceModel() { }

    public BarberServiceModel(String serviceID, String dateCreated, String timezone, String calendar, String local) {
        this.serviceID = serviceID;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
