package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class CombineCustomerBookingModel implements Serializable {

    String serviceImageURL = "", bookedBarberShopName = "", bookingUniqueIDDD = "", bookingTotalTime = "", bookStartTSS = ""
            , bookTzone = "", bookClandar = "", bookLocal = "", shopUniqueID = "";

    public CombineCustomerBookingModel() { }

    public CombineCustomerBookingModel(String serviceImageURL, String bookedBarberShopName, String bookingUniqueIDDD, String bookingTotalTime, String bookStartTSS, String bookTzone, String bookClandar, String bookLocal, String shopUniqueID) {
        this.serviceImageURL = serviceImageURL;
        this.bookedBarberShopName = bookedBarberShopName;
        this.bookingUniqueIDDD = bookingUniqueIDDD;
        this.bookingTotalTime = bookingTotalTime;
        this.bookStartTSS = bookStartTSS;
        this.bookTzone = bookTzone;
        this.bookClandar = bookClandar;
        this.bookLocal = bookLocal;
        this.shopUniqueID = shopUniqueID;
    }

    public String getServiceImageURL() {
        return serviceImageURL;
    }

    public void setServiceImageURL(String serviceImageURL) {
        this.serviceImageURL = serviceImageURL;
    }

    public String getBookedBarberShopName() {
        return bookedBarberShopName;
    }

    public void setBookedBarberShopName(String bookedBarberShopName) {
        this.bookedBarberShopName = bookedBarberShopName;
    }

    public String getBookingUniqueIDDD() {
        return bookingUniqueIDDD;
    }

    public void setBookingUniqueIDDD(String bookingUniqueIDDD) {
        this.bookingUniqueIDDD = bookingUniqueIDDD;
    }

    public String getBookingTotalTime() {
        return bookingTotalTime;
    }

    public void setBookingTotalTime(String bookingTotalTime) {
        this.bookingTotalTime = bookingTotalTime;
    }

    public String getBookStartTSS() {
        return bookStartTSS;
    }

    public void setBookStartTSS(String bookStartTSS) {
        this.bookStartTSS = bookStartTSS;
    }

    public String getBookTzone() {
        return bookTzone;
    }

    public void setBookTzone(String bookTzone) {
        this.bookTzone = bookTzone;
    }

    public String getBookClandar() {
        return bookClandar;
    }

    public void setBookClandar(String bookClandar) {
        this.bookClandar = bookClandar;
    }

    public String getBookLocal() {
        return bookLocal;
    }

    public void setBookLocal(String bookLocal) {
        this.bookLocal = bookLocal;
    }

    public String getShopUniqueID() {
        return shopUniqueID;
    }

    public void setShopUniqueID(String shopUniqueID) {
        this.shopUniqueID = shopUniqueID;
    }
}
