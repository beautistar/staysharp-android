package com.teckdk.cuttiin.Base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.teckdk.cuttiin.R;

/**
 * Created by HGS on 12/11/2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener{

    public Context _context = null;
    private KProgressHUD progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _context = this;
        progressBar = KProgressHUD.create(this).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onDestroy() {

        closeProgress();
        super.onDestroy();
    }

    public void showProgress() {
        progressBar.setCancellable(true);
        progressBar.show();
    }

    public void closeProgress() {
        progressBar.dismiss();
    }

    public void showToast(String toast_string) {
        Toast.makeText(_context, toast_string, Toast.LENGTH_SHORT).show();
    }

    public void showAlertDialog(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    public void changeButtonStyle(Button btn, boolean active){
        btn.setClickable(active);
        if (active){
            btn.setBackground(getResources().getDrawable(R.drawable.bg_btn_active));
            btn.setTextColor(getResources().getColor(R.color.whiteBlue));
        }else {
            btn.setBackground(getResources().getDrawable(R.drawable.bg_btn_unactive));
            btn.setTextColor(getResources().getColor(R.color.blackColor));
        }
    }

    public void changeButtonStyle_1(Button btn, boolean active){
        btn.setClickable(active);
        if (active){
            btn.setAlpha(1.0f);
        }else {
            btn.setAlpha(0.3f);
        }
    }

    @Override
    public void onClick(View v) { }
}
