package com.teckdk.cuttiin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.teckdk.cuttiin.R;

/**
 * Created by STS on 5/27/2018.
 */

public class ServiceListAdapter extends BaseAdapter {

    Context context;

    public ServiceListAdapter(Context _context) {
        this.context = _context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){
            holder = new CustomHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_service_list, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    class  CustomHolder{
        private void setId(View view){

        }
    }
}
