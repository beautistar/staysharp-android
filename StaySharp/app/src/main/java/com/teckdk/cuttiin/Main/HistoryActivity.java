package com.teckdk.cuttiin.Main;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teckdk.cuttiin.Adapter.BookListAdapter;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class HistoryActivity extends BaseActivity {

    BookListAdapter mBookListAdapter;

    ImageView imvBack;
    TextView txvBookingHistory, txvBookingCanceled;
    ListView lstBookList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        loadLayout();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        txvBookingHistory =  (TextView)findViewById(R.id.txvBookingHistory);
        txvBookingHistory.setOnClickListener(this);

        txvBookingCanceled =  (TextView)findViewById(R.id.txvBookingCanceled);
        txvBookingCanceled.setOnClickListener(this);

        mBookListAdapter = new BookListAdapter(this);
        lstBookList =  (ListView)findViewById(R.id.lstBookList);
        lstBookList.setAdapter(mBookListAdapter);
    }

    private void showBookingCanceled() {
        txvBookingCanceled.setTextColor(getResources().getColor(R.color.mainColor));
        txvBookingCanceled.setBackgroundResource(R.drawable.bg_right_cornor_round);

        txvBookingHistory.setTextColor(getResources().getColor(R.color.white));
        txvBookingHistory.setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    private void showBookingHistory() {
        txvBookingHistory.setTextColor(getResources().getColor(R.color.mainColor));
        txvBookingHistory.setBackgroundResource(R.drawable.bg_left_cornor_round);

        txvBookingCanceled.setTextColor(getResources().getColor(R.color.white));
        txvBookingCanceled.setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
            case R.id.txvBookingHistory:
                showBookingHistory();
                break;
            case R.id.txvBookingCanceled:
                showBookingCanceled();
                break;
        }
    }
}
