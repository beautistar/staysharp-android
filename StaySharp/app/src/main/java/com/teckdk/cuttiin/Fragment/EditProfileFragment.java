package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Main.MainCustomActivity;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.BitmapUtils;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.io.File;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class EditProfileFragment extends BaseFragment {

    Context context;

    FirebaseStorage storage;
    StorageReference photoRef;

    ImageView imvBack;
    EditText edtFirstName, edtLastName, edtEmail, edtPhone;
    Button btnSave;
    RoundedImageView imvPhoto;

    String firstName = "", lastName = "", email = "", phoneNumber = "", photoPath = "", photoUrl = "", photoName = "";
    boolean emailChanged = false;
    private Uri imageCaptureUri;

    public EditProfileFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);
        if (!Commons.mUserModel.getProfileImageUrl().equals(Constants.NIL)){
            Picasso.get().load(Commons.mUserModel.getProfileImageUrl()).into(imvPhoto);
        }


        imvBack = (ImageView)fragment.findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        btnSave = (Button)fragment.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnSave.setClickable(false);

        edtFirstName = (EditText)fragment.findViewById(R.id.edtFirstName); edtFirstName.setText(Commons.mUserModel.getFirstName());
        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                firstName = edtFirstName.getText().toString().trim();
                Commons.mUserModel.setFirstName(firstName);
                ((MainCustomActivity)context).changeButtonStyle_1(btnSave, true );
            }
        });

        edtLastName = (EditText)fragment.findViewById(R.id.edtLastName); edtLastName.setText(Commons.mUserModel.getLastName());
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                lastName = edtLastName.getText().toString().trim();
                Commons.mUserModel.setLastName(lastName);
                ((MainCustomActivity)context).changeButtonStyle_1(btnSave, true );
            }
        });

        edtEmail = (EditText)fragment.findViewById(R.id.edtEmail); edtEmail.setText(Commons.mUserModel.getEmail());
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                emailChanged = true;
                email = edtEmail.getText().toString().trim();
                Commons.mUserModel.setEmail(email);
                ((MainCustomActivity)context).changeButtonStyle_1(btnSave, true );
            }
        });

        edtPhone = (EditText)fragment.findViewById(R.id.edtPhone); edtPhone.setText(Commons.mUserModel.getMobileNumber());
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                phoneNumber = edtPhone.getText().toString().trim();
                Commons.mUserModel.setMobileNumber(phoneNumber);
                ((MainCustomActivity)context).changeButtonStyle_1(btnSave, true );
            }
        });

        return fragment;
    }

    private void processUpdate() {

        ((MainCustomActivity)context).showProgress();

        if (emailChanged){
            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            AuthCredential credential = EmailAuthProvider.getCredential(Commons.mUserModel.getEmail(), Commons.password);
            user.reauthenticate(credential)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            user.updateEmail(email)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                LogUtil.e(Constants.TAG + "Email changed");
                                                Preference.getInstance().put(context, PrefConst.PREFKEY_USEREMAIL, email);
                                                Commons.mUserModel.setEmail(email);
                                                updateUserPhoto();
                                            }
                                        }
                                    });
                        }
                    });
        }else {
            updateUserPhoto();
        }
    }

    private void updateUserPhoto(){
        if (photoPath.length() > 0){
            storage = FirebaseStorage.getInstance();
            photoRef = storage.getReference().child(Constants.PROFILE_IMAGES).child(Commons.mUserModel.getUniqueID() + ".jpeg");

            UploadTask uploadTask = photoRef.putFile(imageCaptureUri);
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {

                    if (!task.isSuccessful()) {
                        ((MainCustomActivity)context).closeProgress();
                        LogUtil.e(Constants.TAG + "Failed_00");
                        throw task.getException();
                    }
                    return photoRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                    if (task.isSuccessful()) {
                        String downloadUri = task.getResult().toString();
                        // save photo url
                        Commons.mUserModel.setProfileImageName(photoName);
                        Commons.mUserModel.setProfileImageUrl(downloadUri);

                        updateUserModel();

                    } else {
                        // Handle failures
                        showToast("Upload failed");
                    }
                }
            });
        }else {
            updateUserModel();
        }
    }

    private void updateUserModel(){
        Commons.DATABASE_USER_REF.child(Commons.mUserModel.getUniqueID()).setValue(Commons.mUserModel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        ((MainCustomActivity)context).closeProgress();
                        LogUtil.e(Constants.TAG + "Profile updated");
                        ((MainCustomActivity)context).changeButtonStyle_1(btnSave, false);
                    }
                }).addOnFailureListener(new OnFailureListener() {

                    @Override
                    public void onFailure(@NonNull Exception e) {
                        ((MainCustomActivity)context).closeProgress();
                        showToast("Profile update failed");
            }
        });
    }

    private void selectPicture() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();
                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(context);

                        InputStream in = context.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        photoName = saveFile.getName();

                        ((MainCustomActivity)context).changeButtonStyle_1(btnSave, true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(context, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(context)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                ((MainCustomActivity)context).showProfileFragment();
                break;
            case R.id.btnSave:
                processUpdate();
                break;
            case R.id.imvPhoto:
                selectPicture();
                break;
        }
    }
}
