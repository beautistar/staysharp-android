package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class CustomerModel implements Serializable {
    String uniqueID = "", firstName = "", lastName = "", email = "", role = ""
            , profileImageUrl = "", mobileNumber = "", profileImageName = ""
            , dateAccountCreated = "", timezone = "", calendar = "", local = ""
            , cardID = "", cardDateCreated = "", cardDateCalendar = "" , cardDateTimeZone = ""
            , cardDateLocale = "",  cardHasBeenDeleted = "";

    public CustomerModel(){};

    public CustomerModel(String uniqueID, String firstName, String lastName, String email, String role, String profileImageUrl, String mobileNumber, String profileImageName, String dateAccountCreated, String timezone, String calendar, String local, String cardID, String cardDateCreated, String cardDateCalendar, String cardDateTimeZone, String cardDateLocale, String cardHasBeenDeleted) {
        this.uniqueID = uniqueID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.profileImageUrl = profileImageUrl;
        this.mobileNumber = mobileNumber;
        this.profileImageName = profileImageName;
        this.dateAccountCreated = dateAccountCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
        this.cardID = cardID;
        this.cardDateCreated = cardDateCreated;
        this.cardDateCalendar = cardDateCalendar;
        this.cardDateTimeZone = cardDateTimeZone;
        this.cardDateLocale = cardDateLocale;
        this.cardHasBeenDeleted = cardHasBeenDeleted;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getProfileImageName() {
        return profileImageName;
    }

    public void setProfileImageName(String profileImageName) {
        this.profileImageName = profileImageName;
    }

    public String getDateAccountCreated() {
        return dateAccountCreated;
    }

    public void setDateAccountCreated(String dateAccountCreated) {
        this.dateAccountCreated = dateAccountCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getCardDateCreated() {
        return cardDateCreated;
    }

    public void setCardDateCreated(String cardDateCreated) {
        this.cardDateCreated = cardDateCreated;
    }

    public String getCardDateCalendar() {
        return cardDateCalendar;
    }

    public void setCardDateCalendar(String cardDateCalendar) {
        this.cardDateCalendar = cardDateCalendar;
    }

    public String getCardDateTimeZone() {
        return cardDateTimeZone;
    }

    public void setCardDateTimeZone(String cardDateTimeZone) {
        this.cardDateTimeZone = cardDateTimeZone;
    }

    public String getCardDateLocale() {
        return cardDateLocale;
    }

    public void setCardDateLocale(String cardDateLocale) {
        this.cardDateLocale = cardDateLocale;
    }

    public String getCardHasBeenDeleted() {
        return cardHasBeenDeleted;
    }

    public void setCardHasBeenDeleted(String cardHasBeenDeleted) {
        this.cardHasBeenDeleted = cardHasBeenDeleted;
    }
}
