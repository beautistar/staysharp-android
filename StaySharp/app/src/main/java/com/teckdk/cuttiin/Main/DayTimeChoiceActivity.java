package com.teckdk.cuttiin.Main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.CustomView.NonScrollListView;
import com.teckdk.cuttiin.Models.WorkHoursModel;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;
import com.zcw.togglebutton.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DayTimeChoiceActivity extends BaseActivity {

    final String OPEN = "open", CLOSE = "close";

    SingleDateAndTimePicker tpOpen, tpClose;
    NonScrollListView lstWeek;
    Button btnSave;
    ImageView imvBack;
    TextView txvOpen, txvClosed, txvMinutesBefore;
    ToggleButton tbClosingTime;
    EditText edtMinutesBeforeClosing;

    String selectedDay = "" , openingTimeHour = "", openingTimeMinute = "", openingTimeSecond = "";
    String closingTimeHour = "", closingTimeMinute = "", closingTimeSecond = "", minutesBeforeClosing = "";
    Date openDate, closeDate;
    long openTime = 0, closeTime = 0;
    String dateCreated = "", userTimezone = "", userCalender = "", userLocal = "";

    FirebaseUser currentUser;
    private FirebaseAuth mAuth;

    ArrayList<String> days = new ArrayList<>();
    ArrayList<String> selectedDaysListWeek = new ArrayList<>();

    String openOrClose = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_time_choice);

        init();

        loadLayout();
    }

    @SuppressLint("NewApi")
    private void init() {

        openOrClose = OPEN;

        mAuth =  FirebaseAuth.getInstance();
        currentUser  = mAuth.getCurrentUser();

        selectedDay = getIntent().getStringExtra(Constants.SELECTED_DAY);

        for (String day : Constants.DAYS){
            if (!day.equals(selectedDay)) days.add(day);
        }

        selectedDaysListWeek.add(selectedDay);

        openTime = (Calendar.getInstance().getTime().getTime());
        closeTime = openTime;

        openDate = (Calendar.getInstance()).getTime();
        closeDate = openDate;

        openingTimeHour = String.valueOf(openDate.getHours()); openingTimeMinute = String.valueOf(openDate.getMinutes());
        closingTimeHour = String.valueOf(closeDate.getHours()); closingTimeMinute = String.valueOf(closeDate.getMinutes());

        userTimezone = TimeZone.getDefault().getDisplayName();
        userCalender = android.icu.util.Calendar.getInstance().getType();
        userLocal = Resources.getSystem().getConfiguration().locale.toString();
    }

    private void loadLayout() {

        edtMinutesBeforeClosing = (EditText)findViewById(R.id.edtMinutesBeforeClosing);
        edtMinutesBeforeClosing.setEnabled(false);
        edtMinutesBeforeClosing.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                minutesBeforeClosing = edtMinutesBeforeClosing.getText().toString().trim();
                changeSaveButtonStyle(minutesBeforeClosing.length() > 0 && closingTimeHour.length() > 0
                        && closingTimeMinute.length() > 0 );
            }
        });

        txvMinutesBefore = (TextView)findViewById(R.id.txvMinutesBefore);

        tbClosingTime = (ToggleButton)findViewById(R.id.tbClosingTime);
        tbClosingTime.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on){
                    txvMinutesBefore.setVisibility(View.VISIBLE);
                    edtMinutesBeforeClosing.setEnabled(true);
                }else {
                    txvMinutesBefore.setVisibility(View.INVISIBLE);
                    edtMinutesBeforeClosing.setEnabled(false);
                    edtMinutesBeforeClosing.setText("");
                }
            }
        });

        txvOpen = (TextView)findViewById(R.id.txvOpen);txvOpen.setOnClickListener(this);
        txvClosed = (TextView)findViewById(R.id.txvClosed); txvClosed.setOnClickListener(this);

        tpOpen = (SingleDateAndTimePicker)findViewById(R.id.tpOpen);
        tpOpen.setIsAmPm(false); tpOpen.setStepMinutes(1);
        tpOpen.addOnDateChangedListener(new SingleDateAndTimePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                openingTimeHour = String.valueOf(date.getHours());
                openingTimeMinute =  String.valueOf(date.getMinutes());
                openingTimeSecond = String.valueOf(date.getSeconds());

                if (date.getTime() > closeTime){

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(closeDate);
                    tpOpen.selectDate(cal);
                    tpOpen.animate();

                    openTime = closeDate.getTime(); openDate = closeDate;
                }else {
                    openTime = date.getTime(); openDate = date;
                }

                changeSaveButtonStyle(minutesBeforeClosing.length() > 0 && closingTimeHour.length() > 0
                    && closingTimeMinute.length() > 0 );
            }
        });

        tpClose = (SingleDateAndTimePicker)findViewById(R.id.tpClose);
        tpClose.setIsAmPm(false); tpClose.setStepMinutes(1);
        tpClose.addOnDateChangedListener(new SingleDateAndTimePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                closingTimeHour = String.valueOf(date.getHours());
                closingTimeMinute = String.valueOf(date.getMinutes());
                closingTimeSecond = String.valueOf(date.getSeconds());

                if (date.getTime() < openTime){

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(openDate);
                    tpClose.selectDate(cal);
                    tpClose.animate();

                    closeTime = openDate.getTime(); closeDate = openDate;
                }else {
                    closeTime = date.getTime(); closeDate = date;
                }

                changeSaveButtonStyle(minutesBeforeClosing.length() > 0 && openingTimeHour.length() > 0 && openingTimeMinute.length()> 0);
            }
        });

        WeekAdapter adapter = new WeekAdapter();
        lstWeek = (NonScrollListView)findViewById(R.id.lstWeek);
        lstWeek.setAdapter(adapter);

        btnSave = (Button)findViewById(R.id.btnSave); btnSave.setClickable(false);
        btnSave.setOnClickListener(this);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);
    }

    private void selectOpenServiceTime(){

        openOrClose = OPEN;

        txvOpen.setTextColor(getResources().getColor(R.color.mainColor));
        txvOpen.setBackgroundResource(R.drawable.bg_left_cornor_round);

        txvClosed.setTextColor(getResources().getColor(R.color.white));
        txvClosed.setBackgroundColor(getResources().getColor(R.color.transparent));
        changeSaveButtonStyle(false);
    }

    private void selectClosedServiceTime(){

        openOrClose = CLOSE;

        txvClosed.setTextColor(getResources().getColor(R.color.mainColor));
        txvClosed.setBackgroundResource(R.drawable.bg_right_cornor_round);

        txvOpen.setTextColor(getResources().getColor(R.color.white));
        txvOpen.setBackgroundColor(getResources().getColor(R.color.transparent));
        changeSaveButtonStyle(true);

    }

    private void changeSaveButtonStyle(boolean active){
        if (active){
            btnSave.setClickable(true);
            btnSave.setBackgroundResource(R.drawable.bg_save_btn_active);
        }else {
            btnSave.setClickable(false);
            btnSave.setBackgroundResource(R.drawable.bg_save_btn_unactive);
        }
    }

    private void processSaveServiceTime() {

        SimpleDateFormat sdf = new SimpleDateFormat("EEE d-MMM-yyyy GGG HH:mm:ss.SSS z");
        dateCreated = sdf.format(new Date());

        Commons.DATABASE_WORK_HOURS_REF.child(currentUser.getUid()).removeValue();

        for (String day : selectedDaysListWeek){
            WorkHoursModel  mWorkHoursModel =  new WorkHoursModel(openingTimeHour, openingTimeMinute,
                    openingTimeSecond, closingTimeHour,  closingTimeMinute, closingTimeSecond,
                    minutesBeforeClosing, dateCreated, userTimezone, userCalender, userLocal);

            Commons.DATABASE_WORK_HOURS_REF.child(currentUser.getUid()).child(day).setValue(mWorkHoursModel);
            showToast("Saved!");
        }

        startActivity(new Intent(DayTimeChoiceActivity.this, OpeningHoursActivity.class)); finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSave:
                if (openOrClose.equals(OPEN)){
                    processSaveServiceTime();
                }else if (openOrClose.equals(CLOSE)){
                    Commons.DATABASE_WORK_HOURS_REF.child(currentUser.getUid()).child(selectedDay).removeValue();
                    showToast("Saved!");
                    startActivity(new Intent(DayTimeChoiceActivity.this
                            , OpeningHoursActivity.class)); finish();
                }
                break;
            case R.id.imvBack:
                startActivity(new Intent(this, OpeningHoursActivity.class)); finish();
                break;
            case R.id.txvOpen:
                selectOpenServiceTime();
                break;
            case R.id.txvClosed:
                selectClosedServiceTime();
                break;
        }
    }

    private class WeekAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return days.size();
        }

        @Override
        public String getItem(int position) {
            return days.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.item_week, parent, false);
            TextView txvWeekName = (TextView)convertView.findViewById(R.id.txvWeekName);
            txvWeekName.setText(days.get(position));
            CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.chkWeek);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        selectedDaysListWeek.add(days.get(position));
                    }else {
                        for (int i =  0; i < selectedDaysListWeek.size(); i++){
                            if (selectedDaysListWeek.get(i).equals(days.get(position))){
                                selectedDaysListWeek.remove(i);
                            }
                        }
                    }
                }
            });
            return convertView;
        }
    }
}
