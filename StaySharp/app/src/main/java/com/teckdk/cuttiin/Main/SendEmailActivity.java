package com.teckdk.cuttiin.Main;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;

public class SendEmailActivity extends BaseActivity {

    Button btnSendEmail;
    ImageView imvBack;
    EditText edtEmail;

    String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);

        loadLayout();
    }

    private void loadLayout() {
        imvBack =  (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        btnSendEmail = (Button)findViewById(R.id.btnSendEmail);
        btnSendEmail.setOnClickListener(this);
        btnSendEmail.setClickable(false);

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                changeButtonStyle(btnSendEmail, email.length() > 0);
            }
        });
    }

    private void resetPassword(){
        showProgress();
        FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        closeProgress();
                        if (task.isSuccessful()) {
                            showToast("Reset password link sent to email");
                            finish();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
            case R.id.btnSendEmail:
                resetPassword();
                break;
        }
    }
}
