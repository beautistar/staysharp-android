package com.teckdk.cuttiin.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.BitmapUtils;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.io.File;
import java.io.InputStream;

public class UploadProfilePictureActivity extends BaseActivity {

    RoundedImageView imvPicture;
    Button btnDone;

    FirebaseStorage storage;
    StorageReference photoRef;
    private DatabaseReference mDatabase;

    private Uri imageCaptureUri;
    String photoPath = "", photoUrl = "", photoName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_profile_picture);

        init();
        loadLayout();
    }

    private void init() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    private void loadLayout() {
        btnDone = (Button)findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);
        btnDone.setClickable(false);

        imvPicture = (RoundedImageView)findViewById(R.id.imvPicture);
        imvPicture.setOnClickListener(this);
    }

    private void selectPicture() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();
                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPicture.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        photoName = saveFile.getName();

                        changeButtonStyle_1(btnDone, true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(this, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadProfilePicture() {
        showProgress();
        storage = FirebaseStorage.getInstance();
        photoRef = storage.getReference().child(Constants.PROFILE_IMAGES).child(Commons.mUserModel.getUniqueID() + ".jpeg");

        UploadTask uploadTask = photoRef.putFile(imageCaptureUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                closeProgress();
                if (!task.isSuccessful()) {
                    LogUtil.e(Constants.TAG + "Failed_00");
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                closeProgress();
                if (task.isSuccessful()) {
                    String downloadUri = task.getResult().toString();

                    mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("profileImageUrl").setValue(downloadUri);
                    mDatabase.child("users").child(Commons.mUserModel.getUniqueID()).child("profileImageName").setValue(photoName);

                    gotoMainCustomActivity();
                } else {
                    // Handle failures
                    showToast("Upload failed");
                }
            }
        });
    }

    private void gotoMainCustomActivity() {

        Commons.DATABASE_USER_REF.child(FirebaseAuth.getInstance().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        closeProgress();

                        Commons.mUserModel =  dataSnapshot.getValue(UserModel.class);

                        Commons.DATABASE_USER_REF.child(FirebaseAuth.getInstance().getUid()).removeEventListener(this);

                        startActivity(new Intent(UploadProfilePictureActivity.this, MainCustomActivity.class)); finish();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        closeProgress();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvPicture:
                selectPicture();
                break;
            case R.id.btnDone:
                uploadProfilePicture();
                break;
        }
    }
}
