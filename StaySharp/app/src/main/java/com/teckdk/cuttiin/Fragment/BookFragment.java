package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.teckdk.cuttiin.Adapter.BookListAdapter;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Main.FeedbackActivity;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class BookFragment extends BaseFragment {

    Context context;
    BookListAdapter mBookListAdapter;

    TextView txvUpcoming, txvCompleted;
    ListView lstBookList;

    public BookFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_book, container, false);

        txvUpcoming = (TextView)fragment.findViewById(R.id.txvUpcoming);
        txvUpcoming.setOnClickListener(this);

        txvCompleted = (TextView)fragment.findViewById(R.id.txvCompleted);
        txvCompleted.setOnClickListener(this);

        mBookListAdapter = new BookListAdapter(context);
        lstBookList = (ListView)fragment.findViewById(R.id.lstBookList);
        lstBookList.setAdapter(mBookListAdapter);
        lstBookList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                context.startActivity(new Intent(context, FeedbackActivity.class));
            }
        });

        return fragment;
    }

    private void loadUpcoming() {
        txvUpcoming.setTextColor(getResources().getColor(R.color.mainColor));
        txvUpcoming.setBackgroundResource(R.drawable.bg_left_cornor_round);

        txvCompleted.setTextColor(getResources().getColor(R.color.white));
        txvCompleted.setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    private void loadCompleted() {
        txvCompleted.setTextColor(getResources().getColor(R.color.mainColor));
        txvCompleted.setBackgroundResource(R.drawable.bg_right_cornor_round);

        txvUpcoming.setTextColor(getResources().getColor(R.color.white));
        txvUpcoming.setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txvUpcoming:
                loadUpcoming();
                break;
            case R.id.txvCompleted:
                loadCompleted();
                break;
        }
    }
}
