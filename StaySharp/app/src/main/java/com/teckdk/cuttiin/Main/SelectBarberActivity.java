package com.teckdk.cuttiin.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.teckdk.cuttiin.Adapter.BarbersListAdapter;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.R;

public class SelectBarberActivity extends BaseActivity{

    BarbersListAdapter mBarbersListAdapter;
    ListView lstBarbers;
    ImageView imvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_barber);

        loadLayout();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        mBarbersListAdapter = new BarbersListAdapter(this);
        lstBarbers = (ListView)findViewById(R.id.lstBarbers);
        lstBarbers.setAdapter(mBarbersListAdapter);
        lstBarbers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(SelectBarberActivity.this, BookActivity.class));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
        }
    }
}
