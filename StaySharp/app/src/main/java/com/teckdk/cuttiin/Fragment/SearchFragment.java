package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.teckdk.cuttiin.Adapter.SearchListAdapter;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Main.BarberShopActivity;
import com.teckdk.cuttiin.Main.HistoryActivity;
import com.teckdk.cuttiin.Main.MainCustomActivity;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class SearchFragment extends BaseFragment {

    Context context;
    SearchListAdapter mSearchListAdapter;

    LinearLayout lytBack;
    ListView lstSearchList;

    public SearchFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_search, container, false);

        mSearchListAdapter =  new SearchListAdapter(context);
        lstSearchList = (ListView)fragment.findViewById(R.id.lstSearchList);
        lstSearchList.setAdapter(mSearchListAdapter);
        lstSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                context.startActivity(new Intent(context, BarberShopActivity.class));
            }
        });

        lytBack = (LinearLayout)fragment.findViewById(R.id.lytBack);
        lytBack.setOnClickListener(this);

        return fragment;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytBack:
                ((MainCustomActivity)context).showBarberShopFragment();
                break;
        }
    }
}
