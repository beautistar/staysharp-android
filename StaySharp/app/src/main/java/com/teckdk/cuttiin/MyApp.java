package com.teckdk.cuttiin;

import android.app.Application;

/**
 * Created by Admin on 24/05/2018.
 */

public class MyApp extends Application {

    private static MyApp instance;

    public static MyApp getApplication() {
        return instance;
    }

    public MyApp() {
        instance = this;
    }

    public static MyApp getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
