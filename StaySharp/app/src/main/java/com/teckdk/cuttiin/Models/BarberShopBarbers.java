package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class BarberShopBarbers implements Serializable {
    String barberStaffID = "", dateCreated = "", timezone = "", calendar = "", local = "";

    public BarberShopBarbers() { }

    public BarberShopBarbers(String barberStaffID, String dateCreated, String timezone, String calendar, String local) {
        this.barberStaffID = barberStaffID;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getBarberStaffID() {
        return barberStaffID;
    }

    public void setBarberStaffID(String barberStaffID) {
        this.barberStaffID = barberStaffID;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
