package com.teckdk.cuttiin.Main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.BarberCashPaymentModel;
import com.teckdk.cuttiin.Models.BarberShopModel;
import com.teckdk.cuttiin.Models.BarberShopVerificationModel;
import com.teckdk.cuttiin.Preferences.PrefConst;
import com.teckdk.cuttiin.Preferences.Preference;
import com.teckdk.cuttiin.R;
import com.teckdk.cuttiin.Utils.LogUtil;

import java.text.SimpleDateFormat;
import android.icu.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.TimeZone;

@SuppressLint("NewApi")
public class RegisterBarbersActivity extends BaseActivity {

    ImageView imvBack;
    EditText edtEmail, edtPassword, edtPhone;
    Button btnContinue;
    TextView txvShowHide, txvViewTermsCondition, txvLogin, txvError;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    String email = "", password = "", mobileNumber = "", role = "", companyName = "", companyLogoImageUrl = ""
            , rating = "", companyLogoImageName = "", companyCoverPhotoName = "", companyCoverPhotoUrl = ""
            , accountNumerRegNr = "", accountNumerKontoNr = "", companyFormattedAddress = "", companyAddressLongitude = ""
            , companyAddressLatitude = "", dateAccountCreated = "", userTimezone = "", userCalender = "", userLocal = "", crCode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_barbers);

        init();
        loadLayout();
    }

    private void init() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        companyName = Constants.NIL;
        role = Constants.USER_BARBER_SHOP;
        companyLogoImageUrl = Constants.NIL;
        rating = "0";
        companyLogoImageName = Constants.NIL;
        companyCoverPhotoName = Constants.NIL;
        companyCoverPhotoUrl = Constants.NIL;
        accountNumerRegNr =  Constants.NIL;
        accountNumerKontoNr = Constants.NIL;
        companyFormattedAddress = Constants.NIL;
        companyAddressLongitude = Constants.NIL;
        companyAddressLatitude = Constants.NIL;
        userTimezone = TimeZone.getDefault().getDisplayName();
        userCalender = Calendar.getInstance().getType();
        userLocal = Resources.getSystem().getConfiguration().locale.toString();
        crCode = Currency.getInstance(getResources().getConfiguration().locale).getCurrencyCode();

        SimpleDateFormat sdf = new SimpleDateFormat("EEE d-MMM-yyyy GGG HH:mm:ss.SSS z");
        dateAccountCreated = sdf.format(new Date());
    }

    private void loadLayout() {

        txvError = (TextView)findViewById(R.id.txvError);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                email = edtEmail.getText().toString().trim();
                changeButtonStyle(btnContinue, email.length() > 0 && mobileNumber.length() > 0 && password.length() > 0);
            }
        });

        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                password =  edtPassword.getText().toString().trim();
                changeButtonStyle(btnContinue, email.length() > 0 && mobileNumber.length() > 0 && password.length() > 0);
            }
        });

        edtPhone = (EditText)findViewById(R.id.edtPhone);
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                mobileNumber = edtPhone.getText().toString().trim();
                changeButtonStyle(btnContinue, email.length() > 0 && mobileNumber.length() > 0 && password.length() > 0);
            }
        });

        btnContinue =  (Button)findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(this);
        btnContinue.setClickable(false);

        txvShowHide =  (TextView)findViewById(R.id.txvShowHide);
        txvShowHide.setOnClickListener(this);

        txvViewTermsCondition = (TextView)findViewById(R.id.txvViewTermsCondition);
        txvViewTermsCondition.setOnClickListener(this);

        txvLogin =  (TextView)findViewById(R.id.txvLogin);
        txvLogin.setOnClickListener(this);
    }

    private void processSignup() {
        showProgress();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            LogUtil.e(Constants.TAG + "Signup success");
                            Preference.getInstance().put(RegisterBarbersActivity.this, PrefConst.PREFKEY_USERPWD, password);
                            Commons.password = password;
                            sendVerificationEmail();
                        }else {
                            closeProgress();
                            txvError.setText(task.getException().getMessage());
                        }
                    }
                });
    }

    private void sendVerificationEmail() {
        final  FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            showToast("Verification email is sent");
                            saveUserInFirebase(user.getUid());
                        }else {
                            closeProgress();
                            txvError.setText(task.getException().getMessage());
                            changeButtonStyle(btnContinue, false);
                        }
                    }
                });
    }

    private void saveUserInFirebase(final String uid) {
        final BarberShopModel mBarberShopModel = new BarberShopModel(uid, companyName, email, role, companyLogoImageUrl, mobileNumber,
                rating, companyLogoImageName, companyCoverPhotoName, companyCoverPhotoUrl, accountNumerRegNr,
                accountNumerKontoNr, companyFormattedAddress, companyAddressLongitude, companyAddressLatitude,
                dateAccountCreated, userTimezone, userCalender, userLocal, crCode);
        mDatabase.child("users").child(uid).setValue(mBarberShopModel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        closeProgress();
                        LogUtil.e(Constants.TAG + "UID == " + uid);
                        Commons.mBarberModel = mBarberShopModel;

                        createPersonalBookingCounter(uid);
                        createBarberCashPaymentVerification(uid);
                        createBarberShopVerification(uid);

                        Preference.getInstance().put(RegisterBarbersActivity.this, PrefConst.PREFKEY_USEREMAIL, email);
                        Preference.getInstance().put(RegisterBarbersActivity.this, PrefConst.PREFKEY_USERPWD, password);

                        startActivity(new Intent(RegisterBarbersActivity.this, ChoosePhotoActivity.class)); finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        closeProgress();
                        txvError.setText(e.getMessage());
                    }
                });
    }

    private void createBarberShopVerification(String barberShopID) {
        BarberShopVerificationModel mBarberShopVerificationModel = new BarberShopVerificationModel(barberShopID, Constants.NO, dateAccountCreated, userTimezone, userCalender, userLocal);
        Commons.BARBER_SHOP_VERIFICATION_REF.child(barberShopID).setValue(mBarberShopVerificationModel);
    }

    private void createPersonalBookingCounter(String barberShopID) {
        Commons.TOTAL_NUMBER_OF_BOOKINGS_REF.child(barberShopID).child("numberOfBookings").setValue("0");
    }

    private void createBarberCashPaymentVerification(String barberShopID) {
        BarberCashPaymentModel mBarberCashPaymentModel = new BarberCashPaymentModel(barberShopID, Constants.NO, dateAccountCreated, userTimezone, userCalender, userLocal);
        Commons.CREATE_PAYMENT_VERIFICATION_REF.child(barberShopID).setValue(mBarberCashPaymentModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
            case R.id.btnContinue:
                txvError.setText("");
                processSignup();
                break;
            case R.id.txvShowHide:
                txvShowHide.setSelected(!txvShowHide.isSelected());
                if (txvShowHide.isSelected()){
                    txvShowHide.setText("Hide");
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                }else {
                    txvShowHide.setText("Show");
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                break;
            case R.id.txvViewTermsCondition:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://teckdk.com/"));
                startActivity(browserIntent);
                break;
            case R.id.txvLogin:
                startActivity(new Intent(this, LoginActivity.class)); finish();
                break;
        }
    }
}
