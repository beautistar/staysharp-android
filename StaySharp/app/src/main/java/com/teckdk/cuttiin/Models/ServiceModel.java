package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class ServiceModel implements Serializable {
    String serviceID = "", serviceTitle = "", estimatedTime = "", serviceCost = ""
            , serviceCostLocal = "", shortDescription = "", category = "", serviceImageName = ""
            , serviceImageUrl = "", dateCreated = "", timezone = "", calendar = "", local = ""
            , localCurrency = "", isDeleted = "";

    public ServiceModel() { }

    public ServiceModel(String serviceID, String serviceTitle, String estimatedTime, String serviceCost, String serviceCostLocal, String shortDescription, String category, String serviceImageName, String serviceImageUrl, String dateCreated, String timezone, String calendar, String local, String localCurrency, String isDeleted) {
        this.serviceID = serviceID;
        this.serviceTitle = serviceTitle;
        this.estimatedTime = estimatedTime;
        this.serviceCost = serviceCost;
        this.serviceCostLocal = serviceCostLocal;
        this.shortDescription = shortDescription;
        this.category = category;
        this.serviceImageName = serviceImageName;
        this.serviceImageUrl = serviceImageUrl;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
        this.localCurrency = localCurrency;
        this.isDeleted = isDeleted;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getServiceCost() {
        return serviceCost;
    }

    public void setServiceCost(String serviceCost) {
        this.serviceCost = serviceCost;
    }

    public String getServiceCostLocal() {
        return serviceCostLocal;
    }

    public void setServiceCostLocal(String serviceCostLocal) {
        this.serviceCostLocal = serviceCostLocal;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getServiceImageName() {
        return serviceImageName;
    }

    public void setServiceImageName(String serviceImageName) {
        this.serviceImageName = serviceImageName;
    }

    public String getServiceImageUrl() {
        return serviceImageUrl;
    }

    public void setServiceImageUrl(String serviceImageUrl) {
        this.serviceImageUrl = serviceImageUrl;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getLocalCurrency() {
        return localCurrency;
    }

    public void setLocalCurrency(String localCurrency) {
        this.localCurrency = localCurrency;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
}
