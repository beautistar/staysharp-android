package com.teckdk.cuttiin.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Base.BaseFragment;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Main.MainCustomActivity;
import com.teckdk.cuttiin.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class CustomerProfileFragment extends BaseFragment {

    Context context;

    LinearLayout lytProfile, lytSetting;
    RoundedImageView imvPhoto;
    TextView txvUserName, txvUid;

    public CustomerProfileFragment(Context _context) {
        this.context = _context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_profile, container, false);

        lytProfile = (LinearLayout)fragment.findViewById(R.id.lytProfile);
        lytProfile.setOnClickListener(this);

        lytSetting = (LinearLayout)fragment.findViewById(R.id.lytSetting);
        lytSetting.setOnClickListener(this);

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);
        if (!Commons.mUserModel.getProfileImageUrl().equals(Constants.NIL)){
            Picasso.get().load(Commons.mUserModel.getProfileImageUrl()).into(imvPhoto);
        }

        txvUserName = (TextView)fragment.findViewById(R.id.txvUserName);
        txvUserName.setText(Commons.mUserModel.getFirstName() + " " + Commons.mUserModel.getLastName()
        );

        txvUid = (TextView)fragment.findViewById(R.id.txvUid);
        txvUid.setText(txvUid.getText() + " " + Commons.mUserModel.getUniqueID());

        return fragment;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytProfile:
                ((MainCustomActivity)context).showEditProfileFragment();
                break;
            case R.id.lytSetting:
                ((MainCustomActivity)context).showSettingFragment();
                break;

        }
    }
}
