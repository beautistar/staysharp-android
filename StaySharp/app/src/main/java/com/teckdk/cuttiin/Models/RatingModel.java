package com.teckdk.cuttiin.Models;

import java.io.Serializable;

public class RatingModel implements Serializable {
    String bookingID = "", barberShopID = "", barberID = "", customerID = "", barberRatingVale = ""
            , barberShopRatingValue = "", barberRatingComment = "", barberShopRatingComment = ""
            , dateCreated = "", timezone = "", calendar = "", local = "";

    public RatingModel() { }

    public RatingModel(String bookingID, String barberShopID, String barberID, String customerID, String barberRatingVale, String barberShopRatingValue, String barberRatingComment, String barberShopRatingComment, String dateCreated, String timezone, String calendar, String local) {
        this.bookingID = bookingID;
        this.barberShopID = barberShopID;
        this.barberID = barberID;
        this.customerID = customerID;
        this.barberRatingVale = barberRatingVale;
        this.barberShopRatingValue = barberShopRatingValue;
        this.barberRatingComment = barberRatingComment;
        this.barberShopRatingComment = barberShopRatingComment;
        this.dateCreated = dateCreated;
        this.timezone = timezone;
        this.calendar = calendar;
        this.local = local;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getBarberShopID() {
        return barberShopID;
    }

    public void setBarberShopID(String barberShopID) {
        this.barberShopID = barberShopID;
    }

    public String getBarberID() {
        return barberID;
    }

    public void setBarberID(String barberID) {
        this.barberID = barberID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getBarberRatingVale() {
        return barberRatingVale;
    }

    public void setBarberRatingVale(String barberRatingVale) {
        this.barberRatingVale = barberRatingVale;
    }

    public String getBarberShopRatingValue() {
        return barberShopRatingValue;
    }

    public void setBarberShopRatingValue(String barberShopRatingValue) {
        this.barberShopRatingValue = barberShopRatingValue;
    }

    public String getBarberRatingComment() {
        return barberRatingComment;
    }

    public void setBarberRatingComment(String barberRatingComment) {
        this.barberRatingComment = barberRatingComment;
    }

    public String getBarberShopRatingComment() {
        return barberShopRatingComment;
    }

    public void setBarberShopRatingComment(String barberShopRatingComment) {
        this.barberShopRatingComment = barberShopRatingComment;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
