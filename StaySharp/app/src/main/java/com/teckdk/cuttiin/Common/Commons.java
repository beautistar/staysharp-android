package com.teckdk.cuttiin.Common;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.teckdk.cuttiin.Models.BarberShopModel;
import com.teckdk.cuttiin.Models.CustomerModel;
import com.teckdk.cuttiin.Models.UserModel;

/**
 * Created by Li Ming on 11/21/2017.
 */

public class Commons {

    public static String phoneUid = "";
    public static double lat = 0.0f;
    public static double lng = 0.0f;
    public static boolean saveHours =  false;
    public static String userType = "";
    public static BarberShopModel mBarberModel = new BarberShopModel();
    public static UserModel mUserModel = new UserModel();
    public static DatabaseReference DATABASE_USER_REF = FirebaseDatabase.getInstance().getReference().child("users");
    public static DatabaseReference DATABASE_WORK_HOURS_REF = FirebaseDatabase.getInstance().getReference().child("workhours");
    public static DatabaseReference CREATE_PAYMENT_VERIFICATION_REF = FirebaseDatabase.getInstance().getReference().child("cashPaymentVerification");
    public static DatabaseReference TOTAL_NUMBER_OF_BOOKINGS_REF = FirebaseDatabase.getInstance().getReference().child("totalNumberOfBookings");
    public static DatabaseReference BARBER_SHOP_VERIFICATION_REF = FirebaseDatabase.getInstance().getReference().child("barberShopVerification");
    public static DatabaseReference SERVICE_REF = FirebaseDatabase.getInstance().getReference().child("service");
    public static DatabaseReference RATING_REF = FirebaseDatabase.getInstance().getReference().child("rating");
    public static DatabaseReference BARBERSHOP_BARBERS_REF = FirebaseDatabase.getInstance().getReference().child("barberShop-Barbers");
    public static DatabaseReference SERVICE_BARBERS_REF = FirebaseDatabase.getInstance().getReference().child("serviceBarbers");
    public static DatabaseReference BARBER_SERVICE_REF = FirebaseDatabase.getInstance().getReference().child("barberServices");
    public static DatabaseReference BOOKING_REF = FirebaseDatabase.getInstance().getReference().child("bookings");
    public static DatabaseReference PAYMENTS_REF = FirebaseDatabase.getInstance().getReference().child("payments");

    public static String password = "";
    public static boolean loggedIn = false;
}
