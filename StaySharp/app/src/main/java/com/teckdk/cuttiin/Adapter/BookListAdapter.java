package com.teckdk.cuttiin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.CombineCustomerBookingModel;
import com.teckdk.cuttiin.R;

import java.util.ArrayList;

/**
 * Created by STS on 5/27/2018.
 */

public class BookListAdapter extends BaseAdapter {

    Context context;

    ArrayList<CombineCustomerBookingModel> allData = new ArrayList<>();

    public BookListAdapter(Context _context) {
        this.context = _context;
    }

    public void clearAll(){
        allData.clear();
        notifyDataSetChanged();
    }

    public void add(CombineCustomerBookingModel data1){
        allData.add(data1);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public CombineCustomerBookingModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){
            holder = new CustomHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_book_list, parent, false);

            holder.setId(convertView);
            holder.setData(allData.get(position));

            convertView.setTag(holder);

        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    class  CustomHolder{
        RoundedImageView imvPhoto;
        TextView txvServiceTime, txvUserName, txvEstimatedTime;

        private void setId(View view){
            imvPhoto = (RoundedImageView)view.findViewById(R.id.imvPhoto);
            txvServiceTime = (TextView)view.findViewById(R.id.txvServiceTime);
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            txvEstimatedTime = (TextView)view.findViewById(R.id.txvEstimatedTime);
        }

        private void setData(CombineCustomerBookingModel mCombineCustomerBooking){
            if (!mCombineCustomerBooking.getServiceImageURL().equals(Constants.NIL) && mCombineCustomerBooking.getServiceImageURL().length() > 0){
                Picasso.get().load(mCombineCustomerBooking.getServiceImageURL()).into(imvPhoto);
            }

            txvUserName.setText(mCombineCustomerBooking.getBookedBarberShopName());
            txvEstimatedTime.setText(mCombineCustomerBooking.getBookingTotalTime() + "min");
            txvServiceTime.setText(mCombineCustomerBooking.getBookStartTSS());
        }
    }
}
