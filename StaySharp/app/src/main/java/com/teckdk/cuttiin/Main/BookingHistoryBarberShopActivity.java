package com.teckdk.cuttiin.Main;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.teckdk.cuttiin.Adapter.BookListAdapter;
import com.teckdk.cuttiin.Base.BaseActivity;
import com.teckdk.cuttiin.Common.Commons;
import com.teckdk.cuttiin.Common.Constants;
import com.teckdk.cuttiin.Models.Bookings;
import com.teckdk.cuttiin.Models.CombineCustomerBookingModel;
import com.teckdk.cuttiin.Models.Payments;
import com.teckdk.cuttiin.Models.ServiceModel;
import com.teckdk.cuttiin.Models.UserModel;
import com.teckdk.cuttiin.R;

public class BookingHistoryBarberShopActivity extends BaseActivity {

    BookListAdapter mBookHistoryAdapter;
    BookListAdapter mCancelledHistoryAdapter;

    ImageView imvBack;
    TextView txvBookingHistory, txvBookingCanceled;
    ListView lstBookHistory, lstCancelledHistory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history_barber_shop);

        loadLayout();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        txvBookingHistory =  (TextView)findViewById(R.id.txvBookingHistory);
        txvBookingHistory.setOnClickListener(this);

        txvBookingCanceled =  (TextView)findViewById(R.id.txvBookingCanceled);
        txvBookingCanceled.setOnClickListener(this);

        mBookHistoryAdapter = new BookListAdapter(this);
        lstBookHistory =  (ListView)findViewById(R.id.lstBookHistory);
        lstBookHistory.setAdapter(mBookHistoryAdapter);

        mCancelledHistoryAdapter = new BookListAdapter(this);
        lstCancelledHistory = (ListView)findViewById(R.id.lstCancelledHistory);
        lstCancelledHistory.setAdapter(mCancelledHistoryAdapter);
    }

    private void showCanceledBookingHistory() {
        txvBookingCanceled.setTextColor(getResources().getColor(R.color.mainColor));
        txvBookingCanceled.setBackgroundResource(R.drawable.bg_right_cornor_round);

        txvBookingHistory.setTextColor(getResources().getColor(R.color.white));
        txvBookingHistory.setBackgroundColor(getResources().getColor(R.color.transparent));

        lstBookHistory.setVisibility(View.GONE);
        lstCancelledHistory.setVisibility(View.VISIBLE);
    }

    private void showBookingHistory() {
        txvBookingHistory.setTextColor(getResources().getColor(R.color.mainColor));
        txvBookingHistory.setBackgroundResource(R.drawable.bg_left_cornor_round);

        txvBookingCanceled.setTextColor(getResources().getColor(R.color.white));
        txvBookingCanceled.setBackgroundColor(getResources().getColor(R.color.transparent));

        lstBookHistory.setVisibility(View.VISIBLE);
        lstCancelledHistory.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBookHistoryAdapter.clearAll();
        mCancelledHistoryAdapter.clearAll();

        getBookingDataAsAppointments();

        getCancelledBookings();
    }

    private void getCancelledBookings() {
        Commons.BOOKING_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                            Bookings mBooking = postSnapshot.getValue(Bookings.class);

                            final String customerIDXOX = mBooking.getBookedBarberShopID(),
                                    userIDXOX = Commons.mUserModel.getUniqueID(),
                                    payedBook = mBooking.getPaymentID(),
                                    barberIDX = mBooking.getBookedBarberID(),
                                    bookStartString = mBooking.getBookingStartTime(),
                                    bookClandar = mBooking.getCalendar(),
                                    bookTizne = mBooking.getTimezone(),
                                    bookLocal = mBooking.getLocal(),
                                    bookServiceUUID = mBooking.getBookedServiceID(),
                                    bookkUniqueIDX = mBooking.getBookingID(),
                                    bookTotalTmx = getConfirmedTotalTime(postSnapshot.getValue().toString()),
                                    shopID = mBooking.getBookedBarberShopID(),
                                    checkComplete = mBooking.getIsCompleted(),
                                    isCancelled = mBooking.getBookingCancel();

                            if (customerIDXOX.equals(userIDXOX) && !payedBook.equals(Constants.NIL) && checkComplete.equals(Constants.YES) && isCancelled.equals(Constants.YES)){

                                Commons.PAYMENTS_REF.child(shopID).child(payedBook)
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                Payments mPayment = dataSnapshot.getValue(Payments.class);

                                                String payConfirm = mPayment.getPayment_aq_status_msg();

                                                if (payConfirm.equals(Constants.APPROVED)){
                                                    Commons.DATABASE_USER_REF.child(barberIDX)
                                                            .addValueEventListener(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                                    UserModel mUser = dataSnapshot.getValue(UserModel.class);

                                                                    final String barberShopName = mUser.getBarberShopID(),
                                                                            barberName = mUser.getFirstName();

                                                                    Commons.SERVICE_REF.child(barberShopName).child(bookServiceUUID)
                                                                            .addValueEventListener(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                                                    ServiceModel mService = dataSnapshot.getValue(ServiceModel.class);
                                                                                    String serviceImageURL = mService.getServiceImageUrl();

                                                                                    CombineCustomerBookingModel mCombineCustomerBooking = new CombineCustomerBookingModel(
                                                                                            serviceImageURL, barberName, bookkUniqueIDX, bookTotalTmx, bookStartString,
                                                                                            bookTizne, bookClandar, bookLocal, barberShopName);

                                                                                    mCancelledHistoryAdapter.add(mCombineCustomerBooking);
                                                                                }

                                                                                @Override
                                                                                public void onCancelled(@NonNull DatabaseError databaseError) { }
                                                                            });
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) { }
                                                            });
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                                        });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private void getBookingDataAsAppointments() {

        showProgress();

        Commons.BOOKING_REF.child(Commons.mUserModel.getUniqueID())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot :  dataSnapshot.getChildren()){

                            Bookings mBooking = postSnapshot.getValue(Bookings.class);
                            final String customerIDXOX = mBooking.getBookedBarberShopID(),
                                    userIDXOX = Commons.mUserModel.getUniqueID(),
                                    payedBook = mBooking.getPaymentID(),
                                    barberIDX = mBooking.getBookedBarberID(),
                                    bookStartString = mBooking.getBookingStartTime(),
                                    bookClandar = mBooking.getCalendar(),
                                    bookTizne = mBooking.getTimezone(),
                                    bookLocal = mBooking.getLocal(),
                                    bookServiceUUID = mBooking.getBookedServiceID(),
                                    bookkUniqueIDX =  mBooking.getBookingID(),
                                    bookTotalTmx = getConfirmedTotalTime(postSnapshot.getValue().toString()),
                                    checkComplete = mBooking.getIsCompleted(),
                                    shopID = mBooking.getBookedBarberShopID();

                            if (customerIDXOX.equals(userIDXOX) && !payedBook.equals(Constants.NIL) && checkComplete.equals(Constants.YES)){
                                Commons.PAYMENTS_REF.child(shopID).child(payedBook)
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                Payments mPayment = dataSnapshot.getValue(Payments.class);

                                                String payConfirm =  mPayment.getPayment_qp_status_msg(),
                                                        payCapture = mPayment.getCapture_aq_status_msg();

                                                if (payConfirm.equals(Constants.APPROVED) && payCapture.equals(Constants.APPROVED)){
                                                    Commons.DATABASE_USER_REF.child(barberIDX)
                                                            .addValueEventListener(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    UserModel mUser = dataSnapshot.getValue(UserModel.class);
                                                                    final String  barberShopName = mUser.getBarberShopID(),
                                                                            barberName = mUser.getFirstName();
                                                                    Commons.SERVICE_REF.child(barberShopName).child(bookServiceUUID)
                                                                            .addValueEventListener(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                    ServiceModel mService = dataSnapshot.getValue(ServiceModel.class);

                                                                                    CombineCustomerBookingModel mCombineCustomerBooking = new CombineCustomerBookingModel(
                                                                                            mService.getServiceImageUrl(), barberName, bookkUniqueIDX, bookTotalTmx, bookStartString,
                                                                                            bookTizne, bookClandar, bookLocal, barberShopName);

                                                                                    mBookHistoryAdapter.add(mCombineCustomerBooking);
                                                                                }

                                                                                @Override
                                                                                public void onCancelled(@NonNull DatabaseError databaseError) { }
                                                                            });
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) { }
                                                            });
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                                        });
                            }
                        }
                        closeProgress();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private String getConfirmedTotalTime(String temp){
        String[] parsedString = temp.split(",");
        String resultStr = "";

        for (String str : parsedString){
            if (str.contains("ConfirmedTotalTime")){
                resultStr = (str.split("="))[1];
            }
        }

        return resultStr;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                finish();
                break;
            case R.id.txvBookingHistory:
                showBookingHistory();
                break;
            case R.id.txvBookingCanceled:
                showCanceledBookingHistory();
                break;
        }
    }
}
