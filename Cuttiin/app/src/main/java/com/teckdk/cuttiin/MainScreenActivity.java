package com.teckdk.cuttiin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by seyit on 1.05.2018.
 */

public class MainScreenActivity extends AppCompatActivity {
    ViewPager vp;
    TabLayout tl;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainview);
        vp=findViewById(R.id.pagemain);
        vp.setAdapter(new FragmentPagerAdapterMain(getSupportFragmentManager()));
        tl=findViewById(R.id.maintab);
        tl.setupWithViewPager(vp);
        tl.getTabAt(0).setText("Bookings");
        tl.getTabAt(1).setText("Barbershops");
        tl.getTabAt(2).setText("Profile");
        tl.getTabAt(0).setIcon(R.drawable.custombooking);
        tl.getTabAt(1).setIcon(R.drawable.custombarbers);
        tl.getTabAt(2).setIcon(R.drawable.customprofile);
    }
}
