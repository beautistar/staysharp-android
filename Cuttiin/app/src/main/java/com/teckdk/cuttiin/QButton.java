package com.teckdk.cuttiin;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;


public class QButton extends android.support.v7.widget.AppCompatButton {

    public QButton(Context context) {
        super( context );
        setFont();

    }

    public QButton(Context context, AttributeSet attrs) {
        super( context, attrs );
        setFont();
    }

    public QButton(Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setFont();
    }

    private void setFont() {
        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/BebasNeue.otf");
        setTypeface( normal, Typeface.NORMAL );

    }




}