package com.teckdk.cuttiin;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentPagerAdapterMain extends FragmentPagerAdapter {

    public FragmentPagerAdapterMain(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment =null;
        switch (position) {
            case 0:
                fragment = new BookingFragment();
                break;
            case 1:
                fragment = new MapFragment();
                break;
            case 2:
                fragment = new ProfileFragment();
                break;          }
        return fragment;
    }
    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }
}