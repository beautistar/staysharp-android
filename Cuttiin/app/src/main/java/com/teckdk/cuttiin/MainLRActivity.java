package com.teckdk.cuttiin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by seyit on 1.05.2018.
 */

public class MainLRActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.mainlayout);
        super.onCreate(savedInstanceState);
        QButton loginbt=findViewById(R.id.login);
        QButton registerbt=findViewById(R.id.register);
        loginbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),com.teckdk.cuttiin.LoginActivity.class));
            }
        });
        registerbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),com.teckdk.cuttiin.RegisterActivity.class));
            }
        });
    }
}
