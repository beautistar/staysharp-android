package com.teckdk.cuttiin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class BookingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.booking_fragment, container,false);
        RadioGroup rg=view.findViewById(R.id.rgbooking);
        ((RadioButton)rg.getChildAt(0)).setChecked(true);
        LayoutInflater xer = LayoutInflater.from(getContext());
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.bookingitem, null, false);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(),BookingDetails.class));
            }
        });
        ((LinearLayout)view.findViewById(R.id.container)).addView(layout);
        return view;
    }
}